package com.igor2i.kingdomSchool.services.impls;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.db.LessonsInDB;
import com.igor2i.kingdomSchool.db.modelsBO.Lessons;
import com.igor2i.kingdomSchool.services.LessonsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 02.03.17.
 */
@Service
public class LessonsServiceImpl implements LessonsService {

    private static final Logger LOGGER = LogManager.getLogger(LessonsServiceImpl.class);

    @Autowired
    private LessonsInDB lessonsInDB;

    @Override
    public ConcurrentHashMap<Long, Lessons> getAll() throws ResponceExceptionImpl {
        LOGGER.debug("LessonsService call getAll ");
        return lessonsInDB.getAll();
    }
}
