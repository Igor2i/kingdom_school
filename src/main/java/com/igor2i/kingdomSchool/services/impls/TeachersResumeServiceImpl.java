package com.igor2i.kingdomSchool.services.impls;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.db.TeacherResumeInDB;
import com.igor2i.kingdomSchool.db.modelsBO.TeacherResume;
import com.igor2i.kingdomSchool.services.TeachersResumeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 02.03.17.
 */
@Service
public class TeachersResumeServiceImpl implements TeachersResumeService {

    private static final Logger LOGGER = LogManager.getLogger(TeachersResumeServiceImpl.class);

    @Autowired
    private TeacherResumeInDB teacherResumeInDB;

    @Override
    public ConcurrentHashMap<Long, TeacherResume> getAll() throws ResponceExceptionImpl {
        return teacherResumeInDB.getAll();
    }

    @Override
    public void setUpdateTeacherResume(TeacherResume teacherResume) throws ResponceExceptionImpl {
        teacherResumeInDB.setUpdateById(teacherResume.getIduser(), teacherResume);
    }

    @Override
    public TeacherResume getTeacherResumeById(Long idUser) throws ResponceExceptionImpl {
        return teacherResumeInDB.getById(idUser);
    }
}
