package com.igor2i.kingdomSchool.services.impls;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.db.UsersInDB;
import com.igor2i.kingdomSchool.db.modelsBO.User;
import com.igor2i.kingdomSchool.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 24.02.17.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);


    @Autowired
    private UsersInDB usersInDB;


    @Override
    public ConcurrentHashMap<Long, User> getAll() throws ResponceExceptionImpl {
        LOGGER.debug("UserService call getAll ");
            return usersInDB.getAll();
    }

    @Override
    public User setUser(User user) throws ResponceExceptionImpl {
        return usersInDB.setNewField(user);
    }

    @Override
    public void setUpdateUser(User user) throws ResponceExceptionImpl {
        usersInDB.setUpdateById(user.getId(), user);
    }
}
