package com.igor2i.kingdomSchool.services.impls;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.db.ExperienceInDB;
import com.igor2i.kingdomSchool.db.modelsBO.UserExperience;
import com.igor2i.kingdomSchool.services.UserExperienceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 02.03.17.
 */
@Service
public class UserExperienceServiceImpl implements UserExperienceService {

    private static final Logger LOGGER = LogManager.getLogger(UserExperienceServiceImpl.class);

    @Autowired
    private ExperienceInDB experienceInDB;

    @Override
    public ConcurrentHashMap<Long, UserExperience> getAll() throws ResponceExceptionImpl {
        LOGGER.debug("UserExperienceService call getAll ");
        return experienceInDB.getAll();
    }

    @Override
    public void setUpdateUserExp(UserExperience userExperience) throws ResponceExceptionImpl {
        experienceInDB.setUpdateById(userExperience.getIdUser(), userExperience);
    }

    @Override
    public UserExperience getUserExp(Long userId) throws ResponceExceptionImpl {
        return experienceInDB.getById(userId);
    }
}
