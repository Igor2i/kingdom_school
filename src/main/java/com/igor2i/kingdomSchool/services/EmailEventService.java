package com.igor2i.kingdomSchool.services;

import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * Created by igor2i on 27.02.17.
 */
@org.springframework.stereotype.Service
public class EmailEventService {

    private static final Logger LOGGER = LogManager.getLogger(EmailEventService.class);

    private static String username;
    private static String password;
    private static String smtpAuth;
    private static String smtpStarttlsEnable;
    private static String smtpHost;
    private static String smtpPort;


    public EmailEventService() {
        try {
            config();
        } catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
        }
    }

    public void eventLogin(String email, String username) {
        String subject = "Осуществлён вход под учётной записью " + username;
        String text = "Осуществлён вход под учётной записью " + username
                + " если вы не произвоили вход, пойдите и разберитесь.";
        sendMail(email, subject, text);
    }


    public void sendMail(String email, String subject, String text) {
        if (email == null || subject == null || text == null) {
            return;
        }
        LOGGER.debug("Sending email to " + email);

        try {
            setSession();
            Message message = new MimeMessage(setSession());
            message.setFrom(new InternetAddress());
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject(subject);
            message.setText(text);

            Transport.send(message);
            LOGGER.debug("Email sent to " + email);
        } catch (MessagingException e) {
            LOGGER.error(e);
        }
    }


    private Session setSession() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", smtpAuth);
        props.put("mail.smtp.starttls.enable", smtpStarttlsEnable);
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.port", smtpPort);

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        return session;
    }


    /**
     * В корне находится файл с конфигурацией подключения к БД
     * <p>
     * Example:
     * <?xml version="1.0" encoding="UTF-8" standalone="no"?>
     * <!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
     * <properties>
     * <entry key="mail.username">user</entry>
     * <entry key="mail.password">pass</entry>
     * <entry key="mail.smtp.auth">true</entry>
     * <entry key="mail.smtp.starttls.enable">true</entry>
     * <entry key="mail.smtp.host">smtp.gmail.com</entry>
     * <entry key="mail.smtp.port">587</entry>
     * </properties>
     *
     * @throws IOException
     */
    private static void config() throws ResponceExceptionImpl {

        Properties props = new Properties();
        try {
            FileInputStream fis = new FileInputStream("./MailConfig.xml");

            props.loadFromXML(fis);
        } catch (FileNotFoundException | InvalidPropertiesFormatException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.FILE_IO_EXCEPTION, e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.EXCEPTION, e.getMessage());
        }

        username = props.getProperty("mail.username");
        password = props.getProperty("mail.password");
        smtpAuth = props.getProperty("mail.smtp.auth");
        smtpStarttlsEnable = props.getProperty("mail.smtp.starttls.enable");
        smtpHost = props.getProperty("mail.smtp.host");
        smtpPort = props.getProperty("mail.smtp.port");

    }

}
