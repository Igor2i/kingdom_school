package com.igor2i.kingdomSchool.services;

import com.igor2i.kingdomSchool.controller.enums.TypeEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by igor2i on 25.02.17.
 */
@Service
public class EventService {

    private static final Logger LOGGER = LogManager.getLogger(EventService.class);


    public static void addAttribEvent(HttpServletRequest req, TypeEvent typeEvent) {
        req.setAttribute("event", EventService.textAllert(typeEvent));
    }

    public static String textAllert(TypeEvent typeEvent) {
        //TODO сделать обработку типа с выводм в msg event
        LOGGER.debug(typeEvent);
        String msg = typeEvent.getMsg();
        String htmlAllert = "<div class=\"alert alert-warning alert-dismissable\">\n" +
                "                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>\n" +
                "                <strong>Warning</strong> " + msg + "\n" +
                "            </div>";
        return htmlAllert;
    }

}
