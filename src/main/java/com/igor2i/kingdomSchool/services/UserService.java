package com.igor2i.kingdomSchool.services;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.db.modelsBO.User;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 02.03.17.
 */
public interface UserService {

    ConcurrentHashMap<Long, User> getAll() throws ResponceExceptionImpl;

    User setUser(User user) throws ResponceExceptionImpl;

    void setUpdateUser(User user) throws ResponceExceptionImpl;

}
