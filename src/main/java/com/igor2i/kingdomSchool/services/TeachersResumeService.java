package com.igor2i.kingdomSchool.services;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.db.modelsBO.TeacherResume;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 02.03.17.
 */
public interface TeachersResumeService {

    ConcurrentHashMap<Long, TeacherResume> getAll() throws ResponceExceptionImpl;

    void setUpdateTeacherResume(TeacherResume teacherResume) throws ResponceExceptionImpl;

    TeacherResume getTeacherResumeById(Long idUser) throws ResponceExceptionImpl;

}
