package com.igor2i.kingdomSchool.services;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.db.modelsBO.UserExperience;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 02.03.17.
 */
public interface UserExperienceService {

    ConcurrentHashMap<Long, UserExperience> getAll() throws ResponceExceptionImpl;

    void setUpdateUserExp(UserExperience userExperience) throws ResponceExceptionImpl;

    UserExperience getUserExp(Long userId) throws ResponceExceptionImpl;

}
