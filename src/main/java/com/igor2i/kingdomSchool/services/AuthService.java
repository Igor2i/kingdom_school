package com.igor2i.kingdomSchool.services;

import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.controller.listeners.TeachersResumeMemoryContextListener;
import com.igor2i.kingdomSchool.controller.listeners.UserMemoryServletContextListener;
import com.igor2i.kingdomSchool.db.modelsBO.TeacherResume;
import com.igor2i.kingdomSchool.db.modelsBO.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by igor2i on 25.02.17.
 */
@Service
public class AuthService {

    @Autowired
    private UserMemoryServletContextListener userMemoryServletContextListener;
    @Autowired
    private TeachersResumeMemoryContextListener teachersResumeMemoryContextListener;

    public void addAttribAuthUser(HttpServletRequest req) {

        String userKey = (String) req.getSession().getAttribute("userKey");

        if (userKey != null) {
            User user = userMemoryServletContextListener.getUserByUserKey(userKey);
            req.setAttribute("userId", user.getId());
            if (user.getFirstName() != null && user.getFirstName() != null) {
                req.setAttribute("userFIO", user.getFirstName() + " " + user.getLastName());
            } else {
                req.setAttribute("userFIO", user.getLogin());
            }
            req.setAttribute("userRole", user.getRole());
        }
    }

    public void addAttribProfileUser(HttpServletRequest req) throws ResponceExceptionImpl {
        String userKey = (String) req.getSession().getAttribute("userKey");

        if (userKey != null) {
            User user = userMemoryServletContextListener.getUserByUserKey(userKey);
            req.setAttribute("email", user.getEmail() != null ? user.getEmail() : "");

            req.setAttribute("firstname", user.getFirstName() != null ? user.getFirstName() : "");
            req.setAttribute("lastname", user.getLastName() != null ? user.getLastName() : "");

            req.setAttribute("male", user.isMale());
            req.setAttribute("vklink", user.getSocial() != null ? user.getSocial() : "");

            TeacherResume teacherResume = teachersResumeMemoryContextListener.getTeacherResumeByUserId(user.getId());

            req.setAttribute("resumeHead", teacherResume != null && teacherResume.getHeader() != null ? teacherResume.getHeader() : "");
            req.setAttribute("resumeBody", teacherResume != null && teacherResume.getBody() != null ? teacherResume.getBody() : "");
        }else {
            throw new ResponceExceptionImpl(TypeException.SESSION_ERROR);
        }
    }

    public User getAuthUser(HttpServletRequest req) {
        String userKey = (String) req.getSession().getAttribute("userKey");

        if (userKey != null) {
            return userMemoryServletContextListener.getUserByUserKey(userKey);
        }
        return null;
    }
}
