package com.igor2i.kingdomSchool.services;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.db.modelsBO.Lessons;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 02.03.17.
 */
public interface LessonsService {

    ConcurrentHashMap<Long, Lessons> getAll() throws ResponceExceptionImpl;

}
