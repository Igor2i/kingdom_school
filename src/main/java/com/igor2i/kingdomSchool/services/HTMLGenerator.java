package com.igor2i.kingdomSchool.services;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by igor2i on 02.03.17.
 */
@Service
public class HTMLGenerator {

    public void setHeadPage(HttpServletRequest req, String headNamePage) {

        StringBuilder sbHead = new StringBuilder();

        sbHead.append("<head>")
                .append("<title>")
                .append(headNamePage)
                .append("</title>")
                .append("    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">")
                .append("    <meta charset=\"utf-8\">")
                .append("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">")
                .append("    <link href=\"/css/bootstrap.min.css\" rel=\"stylesheet\">")
                .append("    <!--[if lt IE 9]>")
                .append("    <script src=\"//html5shim.googlecode.com/svn/trunk/html5.js\"></script>")
                .append("    <![endif]-->")
                .append("    <link href=\"/css/styles.css\" rel=\"stylesheet\">")
                .append("</head>");

        req.setAttribute("head", sbHead);

    }

    public void setFooterScriptsPage(HttpServletRequest req) {

        StringBuilder sbFooterScripts = new StringBuilder();

        sbFooterScripts
                .append("<script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js\">")
                .append("</script>")
                .append("<script src=\"/js/bootstrap.min.js\"></script>")
                .append("<script src=\"/js/scripts.js\"></script>");

        req.setAttribute("footerScripts", sbFooterScripts);
    }


    public void setAboutModulePage(HttpServletRequest req){

        StringBuilder sbAbout = new StringBuilder();

        sbAbout.append("<div id=\"aboutModal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n")
                .append("    <div class=\"modal-dialog\">\n")
                .append("        <div class=\"modal-content\">\n")
                .append("            <div class=\"modal-header\">\n")
                .append("                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>\n")
                .append("                <h2 class=\"text-center\">О нас</h2>\n")
                .append("            </div>\n")
                .append("            <div class=\"modal-body\">\n")
                .append("                <div class=\"col-md-12 text-center\">\n")
                .append("                    Данный ресурс предназначен для совместного изучения инностранных языков.\n")
                .append("                    <br><br>\n")
                .append("                    Желаем Вам успехов!\n")
                .append("                </div>\n")
                .append("            </div>\n")
                .append("            <div class=\"modal-footer\">\n")
                .append("                <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">OK</button>\n")
                .append("            </div>\n")
                .append("        </div>\n")
                .append("    </div>\n")
                .append("</div>");

        req.setAttribute("aboutModule", sbAbout);
    }

    public void setHeaderFirstMenuPage(HttpServletRequest req){

        StringBuilder sbHeaderFirstMenu = new StringBuilder();

        sbHeaderFirstMenu.append("<nav class=\"navbar navbar-fixed-top header\">\n")
                .append("    <div class=\"col-md-12\">\n")
                .append("        <div class=\"navbar-header\">\n")
                .append("            <a href=\"/\"><img src=\"img/Logo.jpg\" height=\"45px\"></a>\n")
                .append("        </div>\n")
                .append("        <div class=\"collapse navbar-collapse\" id=\"navbar-collapse1\">\n")
                .append("            <ul class=\"nav navbar-nav navbar-right\">\n")
                .append("                <li><a href=\"#\" id=\"btnToggle\" class=\"social-bar\"><i class=\"glyphicon glyphicon-th-large\"></i></a></li>\n")
                .append("                <li><a href=\"#\" class=\"social-bar\"><img src=\"img/socail_f.jpg\" height=\"35px\"></a></li>\n")
                .append("                <li><a href=\"#\" class=\"social-bar\"><img src=\"img/socail_t.jpg\" height=\"35px\"></a></li>\n")
                .append("                <li><a href=\"#\" class=\"social-bar\"><img src=\"img/socail_i.jpg\" height=\"35px\"></a></li>\n")
                .append("            </ul>\n")
                .append("        </div>\n")
                .append("    </div>\n")
                .append("</nav>");

        req.setAttribute("headerFirstMenu", sbHeaderFirstMenu);
    }
}
