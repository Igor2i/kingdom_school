package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * Created by igor2i on 18.02.17.
 */
@Deprecated
final class ConnectionMySQL {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionMySQL.class);

    protected Connection connection;
    protected static ConnectionMySQL db;

    private static String url;
    private static String login;
    private static String passwd;
    private static String driver;


    private ConnectionMySQL() {

        try {
            config();
            Class.forName(driver).newInstance();
            connection = DriverManager.getConnection(url, login, passwd);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }


    protected static synchronized ConnectionMySQL getConnectDB() {
        if (db == null) {
            db = new ConnectionMySQL();
            LOGGER.info(db.connection);
        }
        return db;
    }


    /**
     * В корне находится файл с конфигурацией подключения к БД
     * <p>
     * Example:
     * <?xml version="1.0" encoding="UTF-8" standalone="no"?>
     * <!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
     * <properties>
     * <entry key="jdbc.url">jdbc:mysql://localhost/[database]</entry>
     * <entry key="jdbc.driver">com.mysql.jdbc.Driver</entry>
     * <entry key="jdbc.username">[username]</entry>
     * <entry key="jdbc.password">[password]</entry>
     * </properties>
     *
     * @throws IOException
     */
    private static void config() throws ResponceExceptionImpl {

        Properties props = new Properties();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("./JDBCConfig.xml");
            props.loadFromXML(fis);
        } catch (FileNotFoundException | InvalidPropertiesFormatException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.FILE_IO_EXCEPTION, e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.EXCEPTION, e.getMessage());
        }

        url = props.getProperty("jdbc.url");
        driver = props.getProperty("jdbc.driver");
        login = props.getProperty("jdbc.username");
        passwd = props.getProperty("jdbc.password");

    }

}
