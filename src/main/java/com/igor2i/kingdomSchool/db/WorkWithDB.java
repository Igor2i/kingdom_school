package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 18.02.17.
 */
//@Component
public interface WorkWithDB<T> {

    ConcurrentHashMap<Long, T> getAll() throws ResponceExceptionImpl;

    T getById(Long id) throws ResponceExceptionImpl;

    T setNewField(T object) throws ResponceExceptionImpl;

    void setUpdateById(long id, T object) throws ResponceExceptionImpl;

    void importToDB(List<T> list) throws ResponceExceptionImpl;

}
