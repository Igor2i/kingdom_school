package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.modelsBO.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 21.02.17.
 */
public class MessagesInDB implements WorkWithDB<Message> {

    private static final Logger LOGGER = LogManager.getLogger(MessagesInDB.class);

    private static final String tableName = "messages";

    @Override
    public ConcurrentHashMap<Long, Message> getAll() throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;
        try (Statement query = connection.createStatement()) {
            ResultSet resultSet = query.executeQuery(
                    "SELECT `id`, `room`, `idUserFrom`, `idUserTo`, `timeStamp`, `text` " +
                            "FROM `messages`");

            ConcurrentHashMap<Long, Message> messageConcurrentHashMap = new ConcurrentHashMap<>(512);

            while (resultSet.next()) {

                messageConcurrentHashMap.put(resultSet.getLong("id"),
                        new Message(resultSet.getLong("id"),
                                resultSet.getLong("room"),
                                resultSet.getLong("idUserFrom"),
                                resultSet.getLong("idUserTo"),
                                resultSet.getString("text"),
                                resultSet.getDate("timeStamp"))
                );
            }
            return messageConcurrentHashMap;
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }

    @Override
    public Message getById(Long id) throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;
        try (Statement query = connection.createStatement()) {
            ResultSet resultSet = query.executeQuery(
                    "SELECT `id`, `room`, `idUserFrom`, `idUserTo`, `timeStamp`, `text` " +
                            "FROM `messages`" +
                            "WHERE id = " + id);

            Message message = null;

            while (resultSet.next()) {
                message = new Message(resultSet.getLong("id"),
                        resultSet.getLong("room"),
                        resultSet.getLong("idUserFrom"),
                        resultSet.getLong("idUserTo"),
                        resultSet.getString("text"),
                        resultSet.getDate("timeStamp"));
            }

            return message;
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }

    @Override
    public Message setNewField(Message object) {
        return null;
    }

    @Override
    public void setUpdateById(long id, Message object) {

    }

    @Override
    public void importToDB(List<Message> list) throws ResponceExceptionImpl {

        Connection connection = ConnectionMySQL.getConnectDB().connection;

        String queryLessons = "INSERT INTO `messages` (`id`, `room`, `idUserFrom`, `idUserTo`, `timeStamp`, `text`) " +
                "VALUES (?, ?, ?, ?, ?, ?);";
        try {
            try {
                PreparedStatement pst = connection.prepareStatement(queryLessons);

                for (Message message : list) {

                    pst.setLong(1, message.getId());
                    pst.setLong(2, message.getRoom());
                    pst.setLong(3, message.getIdUserFrom());
                    pst.setLong(4, message.getIdUserTo());
                    pst.setDate(5, message.getTimeStamp());
                    pst.setString(6, message.getText());

                    pst.addBatch();
                }

                int[] count = pst.executeBatch();

            } catch (BatchUpdateException ex) {
                LOGGER.debug(ex.getMessage());
                Reporter.importReport(ex, tableName);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }
}
