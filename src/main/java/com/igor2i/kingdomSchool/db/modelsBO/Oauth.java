package com.igor2i.kingdomSchool.db.modelsBO;

public class Oauth {

    private static final long serialVersionUID = 1L;

    private long idUser;
    private String token;

    public Oauth() {
    }

    public Oauth(long idUser, String token) {
        this.idUser = idUser;
        this.token = token;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
