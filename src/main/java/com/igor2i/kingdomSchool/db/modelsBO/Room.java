package com.igor2i.kingdomSchool.db.modelsBO;

public class Room {
    private static final long serialVersionUID = 1L;

    private long id;
    private boolean del;
    private long owner;
    private int access;
    private String link;

    public Room() {
    }

    public Room(long id, boolean del, long owner, int access, String link) {
        this.id = id;
        this.del = del;
        this.owner = owner;
        this.access = access;
        this.link = link;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean getDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    public long getOwner() {
        return owner;
    }

    public void setOwner(long owner) {
        this.owner = owner;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
