package com.igor2i.kingdomSchool.db.modelsBO;

public class ScenariosDiscussion {

    private static final long serialVersionUID = 1L;

    private long id;
    private boolean del;
    private String linkObject;
    private int rating;

    public ScenariosDiscussion() {
    }

    public ScenariosDiscussion(long id, boolean del, String linkObject, int rating) {
        this.id = id;
        this.del = del;
        this.linkObject = linkObject;
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean getDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    public String getLinkObject() {
        return linkObject;
    }

    public void setLinkObject(String linkObject) {
        this.linkObject = linkObject;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
