package com.igor2i.kingdomSchool.db.modelsBO;

public class Event {

    private static final long serialVersionUID = 1L;

    private long id;
    private long idUserTo;
    private String text;

    public Event() {
    }

    public Event(long id, long idUserTo, String text) {
        this.id = id;
        this.idUserTo = idUserTo;
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdUserTo() {
        return idUserTo;
    }

    public void setIdUserTo(long idUserTo) {
        this.idUserTo = idUserTo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
