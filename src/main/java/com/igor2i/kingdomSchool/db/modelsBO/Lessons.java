package com.igor2i.kingdomSchool.db.modelsBO;

import com.igor2i.kingdomSchool.db.entities.LessonsEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "lesson")
@XmlAccessorType(XmlAccessType.FIELD)
public class Lessons {

    private static final long serialVersionUID = 3L;

    private long id;
    private boolean del;
    private long owner;
    private String type;
    private String header;
    private String shortDescription;
    private String linkObject;
    private int rating;
    private int experience;

    public Lessons() {
    }

    public Lessons(long id, boolean del, long owner, String type, String header, String shortDescription, String linkObject, int rating, int experience) {
        this.id = id;
        this.del = del;
        this.owner = owner;
        this.type = type;
        this.header = header;
        this.shortDescription = shortDescription;
        this.linkObject = linkObject;
        this.rating = rating;
        this.experience = experience;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean getDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    public long getOwner() {
        return owner;
    }

    public void setOwner(long owner) {
        this.owner = owner;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLinkObject() {
        return linkObject;
    }

    public void setLinkObject(String linkObject) {
        this.linkObject = linkObject;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public LessonsEntity convertToEntity(){

        LessonsEntity lessonsEntity = new LessonsEntity();
        lessonsEntity.setId(this.getId());
        lessonsEntity.setDel(this.getDel());
        lessonsEntity.setExperience(this.getExperience());
        lessonsEntity.setHeader(this.getHeader());
        lessonsEntity.setLinkObject(this.getLinkObject());
        lessonsEntity.setOwner(this.getOwner());
        lessonsEntity.setRating(this.getRating());
        lessonsEntity.setShortDescription(this.getShortDescription());
        lessonsEntity.setType(this.getType());

        return lessonsEntity;

    }

    @Override
    public String toString() {
        return "Lessons{" +
                "id=" + id +
                ", del=" + del +
                ", owner=" + owner +
                ", type='" + type + '\'' +
                ", linkObject='" + linkObject + '\'' +
                ", rating=" + rating +
                ", experience=" + experience +
                '}';
    }
}
