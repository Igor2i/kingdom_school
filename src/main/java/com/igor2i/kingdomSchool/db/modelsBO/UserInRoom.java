package com.igor2i.kingdomSchool.db.modelsBO;

public class UserInRoom {
  private long iduser;
  private long idroom;

  public UserInRoom() {
  }

  public UserInRoom(long iduser, long idroom) {
    this.iduser = iduser;
    this.idroom = idroom;
  }

  public long getIduser() {
    return iduser;
  }

  public void setIduser(long iduser) {
    this.iduser = iduser;
  }

  public long getIdroom() {
    return idroom;
  }

  public void setIdroom(long idroom) {
    this.idroom = idroom;
  }
}
