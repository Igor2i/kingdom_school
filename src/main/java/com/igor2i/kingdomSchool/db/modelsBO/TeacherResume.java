package com.igor2i.kingdomSchool.db.modelsBO;

import com.igor2i.kingdomSchool.db.entities.TeacherResumeEntity;

public class TeacherResume {

  private static final long serialVersionUID = 2L;
  private long iduser;
  private String firstName;
  private String lastName;
  private String header;
  private String body;

  public TeacherResume() {
  }

  public TeacherResume(long iduser, String header, String body) {
    this.iduser = iduser;
    this.header = header;
    this.body = body;
  }

  public TeacherResume(long iduser, String firstName, String lastName, String header, String body) {
    this.iduser = iduser;
    this.firstName = firstName;
    this.lastName = lastName;
    this.header = header;
    this.body = body;
  }

  public long getIduser() {
    return iduser;
  }

  public void setIduser(long iduser) {
    this.iduser = iduser;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public TeacherResumeEntity convertToEntity(){

    TeacherResumeEntity teacherResumeEntity = new TeacherResumeEntity();
    teacherResumeEntity.setIdUser(this.getIduser());
    teacherResumeEntity.setBody(this.getBody());
    teacherResumeEntity.setHeader(this.getHeader());

    return teacherResumeEntity;

  }
}
