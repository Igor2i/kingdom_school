package com.igor2i.kingdomSchool.db.modelsBO;

import com.igor2i.kingdomSchool.db.entities.UsersEntity;
import com.igor2i.kingdomSchool.db.entities.UsersInfoEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by igor2i on 18.02.17.
 */
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
//@Component
public class User {

    private static final long serialVersionUID = 2L;

    private long id;
    private String login;
    private String passwd;
    private String email;
    private String userKey;
    private boolean del;
    private boolean blocked;
    private int role;
    private String firstName;
    private String lastName;
    private Date lastAccess;

    private Date dob;

    private boolean male;
    private String social;


    public User() {
    }

    public User(long id, String login, String passwd, String email, String userKey, boolean del, boolean blocked, int role, String firstName, String lastName, Date lastAccess) {
        this.id = id;
        this.login = login;
        this.passwd = passwd;
        this.email = email;
        this.userKey = userKey;
        this.del = del;
        this.blocked = blocked;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastAccess = lastAccess;
    }

    public User(long id, String login, String passwd, String email, String userKey, boolean del, boolean blocked, int role, String firstName, String lastName, Date lastAccess, Date dob, boolean male, String social) {
        this.id = id;
        this.login = login;
        this.passwd = passwd;
        this.email = email;
        this.userKey = userKey;
        this.del = del;
        this.blocked = blocked;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastAccess = lastAccess;
        this.dob = dob;
        this.male = male;
        this.social = social;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public boolean isDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public Date getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
    }

    public UsersEntity convertToEntity(){

        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setId(this.getId());
        usersEntity.setLogin(this.getLogin());
        usersEntity.setPasswd(this.getPasswd());
        usersEntity.setEmail(this.getEmail());
        usersEntity.setUserKey(this.getUserKey());
        usersEntity.setDel(this.isDel());
        usersEntity.setRole(this.getRole());
        usersEntity.setFirstName(this.getFirstName());
        usersEntity.setLastName(this.getLastName());
        usersEntity.setLastAccess(this.getLastAccess());


        UsersInfoEntity usersInfoEntity = new UsersInfoEntity();
        usersInfoEntity.setIdUser(this.getId());
        usersInfoEntity.setDob(this.getDob());
        usersInfoEntity.setMale(this.isMale());
        usersInfoEntity.setSocial(this.getSocial());

        usersEntity.setUsersInfoEntity(usersInfoEntity);
        usersInfoEntity.setUsersEntity(usersEntity);

        return usersEntity;

    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwd='" + passwd + '\'' +
                ", email='" + email + '\'' +
                ", userKey='" + userKey + '\'' +
                ", del=" + del +
                ", blocked=" + blocked +
                ", role=" + role +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", lastAccess=" + lastAccess +
                ", dob=" + dob +
                ", male=" + male +
                ", social='" + social + '\'' +
                '}';
    }
}

