package com.igor2i.kingdomSchool.db.modelsBO;

public class Role {

    private static final long serialVersionUID = 1L;

    private long id;
    private String nameRole;

    public Role() {
    }

    public Role(long id, String nameRole) {
        this.id = id;
        this.nameRole = nameRole;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }
}
