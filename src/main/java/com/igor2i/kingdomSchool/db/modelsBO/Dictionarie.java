package com.igor2i.kingdomSchool.db.modelsBO;

public class Dictionarie {

    private static final long serialVersionUID = 1L;

    private long id;
    private long iduser;
    private String word;

    public Dictionarie() {
    }

    public Dictionarie(long id, long iduser, String word) {
        this.id = id;
        this.iduser = iduser;
        this.word = word;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIduser() {
        return iduser;
    }

    public void setIduser(long iduser) {
        this.iduser = iduser;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
