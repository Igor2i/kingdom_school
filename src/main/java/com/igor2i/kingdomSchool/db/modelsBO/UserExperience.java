package com.igor2i.kingdomSchool.db.modelsBO;

import com.igor2i.kingdomSchool.db.entities.UsersExperienceEntity;

public class UserExperience {

  private static final long serialVersionUID = 2L;

  private long idUser;
  private int experience;

  public UserExperience() {
  }

  public UserExperience(long idUser, int experience) {
    this.idUser = idUser;
    this.experience = experience;
  }

  public long getIdUser() {
    return idUser;
  }

  public void setIdUser(long idUser) {
    this.idUser = idUser;
  }

  public int getExperience() {
    return experience;
  }

  public void setExperience(int experience) {
    this.experience = experience;
  }

  public UsersExperienceEntity convertToEntity(){

    UsersExperienceEntity usersExperienceEntity = new UsersExperienceEntity();
    usersExperienceEntity.setIdUser(this.getIdUser());
    usersExperienceEntity.setExperience(this.getExperience());

    return usersExperienceEntity;

  }

}
