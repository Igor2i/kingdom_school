package com.igor2i.kingdomSchool.db.modelsBO;

import java.sql.Date;

public class Message {

    private static final long serialVersionUID = 1L;

    private long id;
    private long room;
    private long idUserFrom;
    private long idUserTo;
    private String text;
    private Date timeStamp;

    public Message() {
    }

    public Message(long id, long room, long idUserFrom, long idUserTo, String text, Date timeStamp) {
        this.id = id;
        this.room = room;
        this.idUserFrom = idUserFrom;
        this.idUserTo = idUserTo;
        this.text = text;
        this.timeStamp = timeStamp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRoom() {
        return room;
    }

    public void setRoom(long room) {
        this.room = room;
    }

    public long getIdUserFrom() {
        return idUserFrom;
    }

    public void setIdUserFrom(long idUserFrom) {
        this.idUserFrom = idUserFrom;
    }

    public long getIdUserTo() {
        return idUserTo;
    }

    public void setIdUserTo(long idUserTo) {
        this.idUserTo = idUserTo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
