package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.modelsBO.ScenariosDiscussion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 21.02.17.
 */
public class ScenariosDiscussionInDB implements WorkWithDB<ScenariosDiscussion> {

    private static final Logger LOGGER = LogManager.getLogger(ScenariosDiscussionInDB.class);

    private static final String tableName = "scenariosDiscussion";

    @Override
    public ConcurrentHashMap<Long, ScenariosDiscussion> getAll() throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;
        try (Statement query = connection.createStatement()) {
            ResultSet resultSet = query.executeQuery(
                    "SELECT `id`, `del`, `linkObject`, `rating` " +
                            "FROM `scenariosDiscussion` " +
                            "WHERE del = 0");

            ConcurrentHashMap<Long, ScenariosDiscussion> discussionConcurrentHashMap = new ConcurrentHashMap<>(512);

            while (resultSet.next()) {

                discussionConcurrentHashMap.put(resultSet.getLong("id"),
                        new ScenariosDiscussion(resultSet.getLong("id"),
                                resultSet.getBoolean("del"),
                                resultSet.getString("linkObject"),
                                resultSet.getInt("rating"))
                );
            }
            return discussionConcurrentHashMap;
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }

    @Override
    public ScenariosDiscussion getById(Long id) throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;
        try (Statement query = connection.createStatement()) {
            ResultSet resultSet = query.executeQuery(
                    "SELECT `id`, `del`, `linkObject`, `rating` " +
                            "FROM `scenariosDiscussion` " +
                            "WHERE del = 0 AND id = " + id);

            ScenariosDiscussion scenariosDiscussion = null;

            while (resultSet.next()) {
                scenariosDiscussion = new ScenariosDiscussion(resultSet.getLong("id"),
                        resultSet.getBoolean("del"),
                        resultSet.getString("linkObject"),
                        resultSet.getInt("rating"));
            }

            return scenariosDiscussion;
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }

    @Override
    public ScenariosDiscussion setNewField(ScenariosDiscussion object) {
        return null;
    }

    @Override
    public void setUpdateById(long id, ScenariosDiscussion object) {

    }

    @Override
    public void importToDB(List<ScenariosDiscussion> list) throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;

        String queryLessons = "INSERT INTO `scenariosDiscussion` (`id`, `del`, `linkObject`, `rating`) " +
                "VALUES (?, ?, ?, ?);";
        try {
            try {
                PreparedStatement pst = connection.prepareStatement(queryLessons);

                for (ScenariosDiscussion scenariosDiscussion : list) {

                    pst.setLong(1, scenariosDiscussion.getId());
                    pst.setBoolean(2, scenariosDiscussion.getDel());
                    pst.setString(3, scenariosDiscussion.getLinkObject());
                    pst.setInt(3, scenariosDiscussion.getRating());

                    pst.addBatch();
                }

                int[] count = pst.executeBatch();

            } catch (BatchUpdateException ex) {
                LOGGER.debug(ex.getMessage());
                Reporter.importReport(ex, tableName);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }
}
