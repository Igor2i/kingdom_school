package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.modelsBO.Room;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 21.02.17.
 */
public class RoomsInDB implements WorkWithDB<Room> {

    private static final Logger LOGGER = LogManager.getLogger(RoomsInDB.class);

    private static final String tableName = "rooms";

    @Override
    public ConcurrentHashMap<Long, Room> getAll() throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;
        try (Statement query = connection.createStatement()) {
            ResultSet resultSet = query.executeQuery(
                    "SELECT `id`, `del`, `owner`, `access`, `link` " +
                            "FROM `rooms`" +
                            "WHERE `del` = 0");

            ConcurrentHashMap<Long, Room> roomConcurrentHashMap = new ConcurrentHashMap<>(128);

            while (resultSet.next()) {

                roomConcurrentHashMap.put(resultSet.getLong("id"),
                        new Room(resultSet.getLong("id"),
                                resultSet.getBoolean("del"),
                                resultSet.getLong("owner"),
                                resultSet.getInt("access"),
                                resultSet.getString("link"))
                );
            }
            return roomConcurrentHashMap;
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }

    @Override
    public Room getById(Long id) throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;
        try (Statement query = connection.createStatement()) {
            ResultSet resultSet = query.executeQuery(
                    "SELECT `id`, `del`, `owner`, `access`, `link` " +
                            "FROM `rooms`" +
                            "WHERE `del` = 0 AND id = " + id);

            Room room = null;

            while (resultSet.next()) {
                room = new Room(resultSet.getLong("id"),
                        resultSet.getBoolean("del"),
                        resultSet.getLong("owner"),
                        resultSet.getInt("access"),
                        resultSet.getString("link"));
            }

            return room;
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }

    @Override
    public Room setNewField(Room object) {
        return null;
    }

    @Override
    public void setUpdateById(long id, Room object) {

    }

    @Override
    public void importToDB(List<Room> list) throws ResponceExceptionImpl {

        Connection connection = ConnectionMySQL.getConnectDB().connection;

        String queryLessons = "INSERT INTO `rooms` (`id`, `del`, `owner`, `access`, `link`) " +
                "VALUES (?, ?, ?, ?, ?);";
        try {
            try {
                PreparedStatement pst = connection.prepareStatement(queryLessons);

                for (Room room : list) {

                    pst.setLong(1, room.getId());
                    pst.setBoolean(2, room.getDel());
                    pst.setLong(3, room.getOwner());
                    pst.setInt(4, room.getAccess());
                    pst.setString(5, room.getLink());

                    pst.addBatch();
                }

                int[] count = pst.executeBatch();

            } catch (BatchUpdateException ex) {
                LOGGER.debug(ex.getMessage());
                Reporter.importReport(ex, tableName);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }
}
