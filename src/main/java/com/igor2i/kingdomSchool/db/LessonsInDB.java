package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.entities.LessonsEntity;
import com.igor2i.kingdomSchool.db.modelsBO.Lessons;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 21.02.17.
 */
@Component
public class LessonsInDB implements WorkWithDB<Lessons> {

    private static final Logger LOGGER = LogManager.getLogger(LessonsInDB.class);

    private static final String tableName = "lessons";

    @Override
    public ConcurrentHashMap<Long, Lessons> getAll() throws ResponceExceptionImpl {

        ConcurrentHashMap<Long, Lessons> lessonsConcurrentHashMap = new ConcurrentHashMap<>(128);

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        List<LessonsEntity> lessonsEntities = session.createQuery("FROM LessonsEntity WHERE  del = 0").list();

        for (LessonsEntity teacherResumeEntity : lessonsEntities) {
            lessonsConcurrentHashMap.put(teacherResumeEntity.getId(), teacherResumeEntity.convertToBO());
        }

        session.getTransaction().commit();
        session.close();

        return lessonsConcurrentHashMap;

    }

    @Override
    public Lessons getById(Long id) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        List<LessonsEntity> lessonsEntities = session
                .createQuery("FROM LessonsEntity WHERE del = 0 AND id = :id")
                .setParameter("id", id).list();

        session.getTransaction().commit();
        session.close();

        Lessons lessons = null;
        for (LessonsEntity lessonsEntity : lessonsEntities) {
            lessons = lessonsEntity.convertToBO();
        }

        return lessons;

    }

    @Override
    public Lessons setNewField(Lessons object) {
        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        LessonsEntity teacherResumeEntity = object.convertToEntity();
        session.save(teacherResumeEntity);
        session.getTransaction().commit();
        session.close();


        return object;
    }



    @Override
    public void setUpdateById(long id, Lessons object) {
        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        LessonsEntity lessonsEntity = object.convertToEntity();
        session.update(lessonsEntity);

        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void importToDB(List<Lessons> list) throws ResponceExceptionImpl {

        Connection connection = ConnectionMySQL.getConnectDB().connection;

        String queryLessons = "INSERT INTO `lessons` (`id`, `del`, `owner`, `type`, `header`, `shortDescription`, `linkObject`, `rating`, `experience`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            try {
                PreparedStatement pst = connection.prepareStatement(queryLessons);

                for (Lessons lessons : list) {

                    pst.setLong(1, lessons.getId());
                    pst.setBoolean(2, lessons.getDel());
                    pst.setLong(3, lessons.getOwner());
                    pst.setString(4, lessons.getType());
                    pst.setString(5, lessons.getHeader());
                    pst.setString(6, lessons.getShortDescription());
                    pst.setString(7, lessons.getLinkObject());
                    pst.setInt(8, lessons.getRating());
                    pst.setInt(9, lessons.getExperience());

                    pst.addBatch();
                }

                int[] count = pst.executeBatch();

            } catch (BatchUpdateException ex) {
                LOGGER.debug(ex.getMessage());
                Reporter.importReport(ex, tableName);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }

    }

}
