package com.igor2i.kingdomSchool.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.BatchUpdateException;
import java.sql.Statement;

/**
 * Created by igor2i on 21.02.17.
 */
class Reporter {

    private static final Logger LOGGER = LogManager.getLogger(Reporter.class);

    protected static void importReport(BatchUpdateException ex, String tableName) {

        int[] updateCount = ex.getUpdateCounts();

        int count = 1;
        for (int i : updateCount) {
            if (i == Statement.EXECUTE_FAILED) {
                LOGGER.debug("Import into " + tableName + ": Error on request " + count + " - Execute failed");
            } else {
                LOGGER.debug("Import into " + tableName + ": Request " + count + " - OK");
            }
            count++;

        }

    }

}
