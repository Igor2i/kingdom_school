package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.controller.enums.RolesType;
import com.igor2i.kingdomSchool.db.entities.UsersEntity;
import com.igor2i.kingdomSchool.db.modelsBO.TeacherResume;
import com.igor2i.kingdomSchool.db.modelsBO.User;
import com.igor2i.kingdomSchool.db.modelsBO.UserExperience;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 19.02.17.
 */
@Component
public class UsersInDB implements WorkWithDB<User> {

    private static final Logger LOGGER = LogManager.getLogger(UsersInDB.class);

    private static final String tableName = "users";

    @Override
    public ConcurrentHashMap<Long, User> getAll() throws ResponceExceptionImpl {

        ConcurrentHashMap<Long, User> userConcurrentHashMap = new ConcurrentHashMap<>(128);

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        List<UsersEntity> usersEntities = session.createQuery("FROM UsersEntity").list();

        for (UsersEntity usersEntity : usersEntities) {
            userConcurrentHashMap.put(usersEntity.getId(), usersEntity.convertToBO());
        }

        session.getTransaction().commit();
        session.close();

        return userConcurrentHashMap;
    }

    @Override
    public User getById(Long id) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        List<UsersEntity> usersEntities = session
                .createQuery("FROM UsersEntity WHERE del = 0 AND id = " + id).list();

        session.getTransaction().commit();
        session.close();

        User user = null;
        for (UsersEntity usersEntity : usersEntities) {
            user = usersEntity.convertToBO();
        }

        return user;
    }


    public User getByName(String name) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        List<UsersEntity> usersEntities = session
                .createQuery("FROM UsersEntity WHERE del = 0 AND login = :login")
                .setParameter("login", name).list();

        session.getTransaction().commit();
        session.close();

        User user = null;
        for (UsersEntity usersEntity : usersEntities) {
            user = usersEntity.convertToBO();
        }

        return user;

    }

    @Override
    public User setNewField(User user) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        UsersEntity usersEntity = user.convertToEntity();
        user.setId((Long) session.save(usersEntity));
        session.getTransaction().commit();
        session.close();

        if (RolesType.UP_STUDENT.isRole(user.getRole())
                || RolesType.TEACHER.isRole(user.getRole())) {
            setTeacherField(user);
        }

        setUserExp(user);

        return user;

    }

    @Override
    public void setUpdateById(long id, User user) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();
        UsersEntity usersEntity = user.convertToEntity();
        session.update(usersEntity);
        session.getTransaction().commit();
        session.close();

    }

    @Override
    public void importToDB(List<User> listUsers) throws ResponceExceptionImpl {

        Connection connection = ConnectionMySQL.getConnectDB().connection;

        String queryUser = "INSERT INTO `users` (`id`, `login`, `passwd`, `email`, `userKey`, `del`, `blocked`, `role`, `firstName`, `lastName`, `lastAccess`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            try {
                PreparedStatement pst = connection.prepareStatement(queryUser);

                for (User user : listUsers) {

                    pst.setLong(1, user.getId());
                    pst.setString(2, user.getLogin());
                    pst.setString(3, user.getPasswd());
                    pst.setString(4, user.getEmail());
                    pst.setString(5, user.getUserKey());
                    pst.setBoolean(6, user.isDel());
                    pst.setBoolean(7, user.isBlocked());
                    pst.setInt(8, user.getRole());
                    pst.setString(9, user.getFirstName());
                    pst.setString(10, user.getLastName());
                    pst.setDate(11, user.getLastAccess() != null ? new java.sql.Date(user.getLastAccess().getTime()) : null);
                    pst.addBatch();
                }

                int[] count = pst.executeBatch();

            } catch (BatchUpdateException ex) {
                LOGGER.debug(ex.getMessage());
                Reporter.importReport(ex, "users");
            }

            String queryUserInfo = "INSERT INTO `usersInfo` (`idUser`, `dob`, `male`, `social`) " +
                    "VALUES (?, ?, ?, ?);";

            try {
                PreparedStatement pst = connection.prepareStatement(queryUserInfo);

                for (User user : listUsers) {

                    pst.setLong(1, user.getId());
                    pst.setDate(2, user.getDob() != null ? new java.sql.Date(user.getDob().getTime()) : null);
                    pst.setBoolean(3, user.isMale());
                    pst.setString(4, user.getSocial());
                    pst.addBatch();
                }

                int[] count = pst.executeBatch();

            } catch (BatchUpdateException ex) {
                LOGGER.debug(ex.getMessage());
                Reporter.importReport(ex, "usersInfo");
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }


    private void setUserExp(User user) throws ResponceExceptionImpl {
        UserExperience userExperience = new UserExperience(user.getId(), 1);

        ExperienceInDB experienceInDB = new ExperienceInDB();
        experienceInDB.setNewField(userExperience);
    }

    private void setTeacherField(User user) throws ResponceExceptionImpl {
        TeacherResume teacherResume = new TeacherResume();
        teacherResume.setIduser(user.getId());

        TeacherResumeInDB teacherResumeInDB = new TeacherResumeInDB();
        teacherResumeInDB.setNewField(teacherResume);
    }


}
