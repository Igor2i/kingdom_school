package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.entities.TeacherResumeEntity;
import com.igor2i.kingdomSchool.db.modelsBO.TeacherResume;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 21.02.17.
 */
@Component
public class TeacherResumeInDB implements WorkWithDB<TeacherResume> {

    private static final Logger LOGGER = LogManager.getLogger(TeacherResumeInDB.class);

    private static final String tableName = "teacherResume";

    @Override
    public ConcurrentHashMap<Long, TeacherResume> getAll() throws ResponceExceptionImpl {

        ConcurrentHashMap<Long, TeacherResume> teacherResumeConcurrentHashMap = new ConcurrentHashMap<>(128);

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        List<TeacherResumeEntity> teacherResumeEntities = session.createQuery("FROM TeacherResumeEntity ").list();

        for (TeacherResumeEntity teacherResumeEntity : teacherResumeEntities) {
            teacherResumeConcurrentHashMap.put(teacherResumeEntity.getIdUser(), teacherResumeEntity.convertToBO());
        }

        session.getTransaction().commit();
        session.close();

        return teacherResumeConcurrentHashMap;

//        Connection connection = ConnectionMySQL.getConnectDB().connection;
//        try(Statement query = connection.createStatement()) {
//            ResultSet resultSet = query.executeQuery(
//                    "SELECT `idUser`, `header`, `body` " +
//                            "FROM `teacherResume`");
//
//            ConcurrentHashMap<Long, TeacherResume> teacherResumeConcurrentHashMap = new ConcurrentHashMap<>(512);
//
//            while (resultSet.next()) {
//
//                teacherResumeConcurrentHashMap.put(resultSet.getLong("idUser"),
//                        new TeacherResume(resultSet.getLong("idUser"),
//                                resultSet.getString("header"),
//                                resultSet.getString("body"))
//                );
//            }
//            return teacherResumeConcurrentHashMap;
//        }catch (SQLException e){
//            LOGGER.error(e);
//            throw new ResponceExceptionImpl( TypeException.SQL_EXCEPTION, e.getMessage());
//        }
    }

    @Override
    public TeacherResume getById(Long id) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        List<TeacherResumeEntity> teacherResumeEntities = session
                .createQuery("FROM TeacherResumeEntity WHERE idUser = :id")
                .setParameter("id", id).list();

        session.getTransaction().commit();
        session.close();

        TeacherResume teacherResume = null;
        for (TeacherResumeEntity teacherResumeEntity : teacherResumeEntities) {
            teacherResume = teacherResumeEntity.convertToBO();
        }

        return teacherResume;

//        Connection connection = ConnectionMySQL.getConnectDB().connection;
//        try(Statement query = connection.createStatement()) {
//            ResultSet resultSet = query.executeQuery(
//                    "SELECT `idUser`, `header`, `body` " +
//                            "FROM `teacherResume` " +
//                            "WHERE id = " + id);
//
//            TeacherResume resume = null;
//
//            while (resultSet.next()) {
//                resume = new TeacherResume(resultSet.getLong("idUser"),
//                        resultSet.getString("header"),
//                        resultSet.getString("body"));
//            }
//
//            return resume;
//        }catch (SQLException e){
//            LOGGER.error(e);
//            throw new ResponceExceptionImpl( TypeException.SQL_EXCEPTION, e.getMessage());
//        }
    }

    @Override
    public TeacherResume setNewField(TeacherResume resume) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        TeacherResumeEntity teacherResumeEntity = resume.convertToEntity();
        teacherResumeEntity.setHeader("");
        teacherResumeEntity.setBody("");
        session.save(teacherResumeEntity);
        session.getTransaction().commit();
        session.close();


        return resume;

//        Connection connection = ConnectionMySQL.getConnectDB().connection;
//
//        try(Statement query = connection.createStatement()) {
//            query.execute(
//                    "INSERT INTO `teacherResume` (`idUser`, `header`, `body`) " +
//                            "VALUES (" + resume.getIduser() + ", '', '');");
//
//            ResultSet resultSet = query.executeQuery(
//                    "SELECT `idUser`, `header`, `body` " +
//                            "FROM `teacherResume` " +
//                            "WHERE idUser = " + resume.getIduser());
//            TeacherResume teacherResume = null;
//
//            while (resultSet.next()) {
//                teacherResume = new TeacherResume(resultSet.getLong("idUser"),
//                        resultSet.getString("header"),
//                        resultSet.getString("body"));
//            }
//
//            return teacherResume;
//        }catch (SQLException e){
//            LOGGER.error(e);
//            throw new ResponceExceptionImpl( TypeException.SQL_EXCEPTION, e.getMessage());
//        }
    }

    @Override
    public void setUpdateById(long id, TeacherResume resume) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        TeacherResumeEntity teacherResumeEntity = resume.convertToEntity();
        session.update(teacherResumeEntity);

        session.getTransaction().commit();
        session.close();

//        Connection connection = ConnectionMySQL.getConnectDB().connection;
//        try {
//            String queryUpdateResume = "UPDATE `teacherResume` SET `header` = ?, `body` = ? " +
//                    "WHERE `idUser` = " + id + ";";
//            PreparedStatement pst = connection.prepareStatement(queryUpdateResume);
//            pst.setString(1, resume.getHeader());
//            pst.setString(2, resume.getBody());
//            pst.executeUpdate();
//
//
//        } catch (SQLException e) {
//            LOGGER.error(e);
//            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
//        }
    }

    @Override
    public void importToDB(List<TeacherResume> list) throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;

        String queryLessons = "INSERT INTO `teacherResume` (`idUser`, `header`, `body`) " +
                "VALUES (?, ?, ?);";

        try {
            try {
                PreparedStatement pst = connection.prepareStatement(queryLessons);

                for (TeacherResume resume : list) {

                    pst.setLong(1, resume.getIduser());
                    pst.setString(2, resume.getHeader());
                    pst.setString(3, resume.getBody());

                    pst.addBatch();
                }

                int[] count = pst.executeBatch();

            } catch (BatchUpdateException ex) {
                LOGGER.debug(ex.getMessage());
                Reporter.importReport(ex, tableName);
            }
        }catch (SQLException e){
            LOGGER.error(e);
            throw new ResponceExceptionImpl( TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }
}
