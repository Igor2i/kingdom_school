package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.entities.UsersExperienceEntity;
import com.igor2i.kingdomSchool.db.modelsBO.UserExperience;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 21.02.17.
 */
@Component
public class ExperienceInDB implements WorkWithDB<UserExperience> {

    private static final Logger LOGGER = LogManager.getLogger(ExperienceInDB.class);

    private static final String tableName = "usersExperience";

    @Override
    public ConcurrentHashMap<Long, UserExperience> getAll() throws ResponceExceptionImpl {

        ConcurrentHashMap<Long, UserExperience> userExperienceConcurrentHashMap = new ConcurrentHashMap<>(128);

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        List<UsersExperienceEntity> userExperiences = session.createQuery("FROM UsersExperienceEntity ").list();

        for (UsersExperienceEntity usersExperienceEntity : userExperiences) {
            userExperienceConcurrentHashMap.put(usersExperienceEntity.getIdUser(), usersExperienceEntity.convertToBO());
        }

        session.getTransaction().commit();
        session.close();

        return userExperienceConcurrentHashMap;

    }

    @Override
    public UserExperience getById(Long id) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        List<UsersExperienceEntity> usersExperienceEntities = session
                .createQuery("FROM UsersExperienceEntity WHERE idUser = :id")
                .setParameter("id", id).list();

        session.getTransaction().commit();
        session.close();

        UserExperience userExperience = null;
        for (UsersExperienceEntity usersExperienceEntity : usersExperienceEntities) {
            userExperience = usersExperienceEntity.convertToBO();
        }

        return userExperience;
    }

    @Override
    public UserExperience setNewField(UserExperience userExperience) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        UsersExperienceEntity usersExperienceEntity = userExperience.convertToEntity();
        usersExperienceEntity.setExperience(1);
        session.save(usersExperienceEntity);
        session.getTransaction().commit();
        session.close();


        return userExperience;

    }

    @Override
    public void setUpdateById(long id, UserExperience object) throws ResponceExceptionImpl {

        Session session = HibernateConnection.getSessionFactory().openSession();
        session.beginTransaction();

        UsersExperienceEntity usersExperienceEntity = object.convertToEntity();
        session.update(usersExperienceEntity);

        session.getTransaction().commit();
        session.close();

    }

    @Override
    public void importToDB(List<UserExperience> list) throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;

        String queryLessons = "INSERT INTO `usersExperience` (`idUser`, `experience`) " +
                "VALUES (?, ?);";
        try {
            try {
                PreparedStatement pst = connection.prepareStatement(queryLessons);

                for (UserExperience experience : list) {

                    pst.setLong(1, experience.getIdUser());
                    pst.setInt(2, experience.getExperience());

                    pst.addBatch();
                }

                int[] count = pst.executeBatch();

            } catch (BatchUpdateException ex) {
                LOGGER.debug(ex.getMessage());
                Reporter.importReport(ex, tableName);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }
}
