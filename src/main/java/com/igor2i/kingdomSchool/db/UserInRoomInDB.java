package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.modelsBO.UserInRoom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 21.02.17.
 */
public class UserInRoomInDB implements WorkWithDB<List<UserInRoom>> {

    private static final Logger LOGGER = LogManager.getLogger(UserInRoomInDB.class);

    private static final String tableName = "userInRoom";

    @Override
    public ConcurrentHashMap<Long, List<UserInRoom>> getAll() throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;
        try(Statement query = connection.createStatement()) {
            ResultSet resultSet = query.executeQuery(
                    "SELECT `idUser`, `idRoom` " +
                            "FROM `userInRoom`");

            ConcurrentHashMap<Long, List<UserInRoom>> concurrentHashMap = new ConcurrentHashMap<>(128);

            while (resultSet.next()) {

                UserInRoom userInRoom = new UserInRoom(resultSet.getLong("idUser"),
                        resultSet.getLong("idRoom"));

                if (!concurrentHashMap.containsKey(resultSet.getLong("idUser"))) {

                    List<UserInRoom> list = new LinkedList<>();
                    list.add(userInRoom);

                    concurrentHashMap.put(resultSet.getLong("idUser"), list);

                } else {
                    concurrentHashMap.get(resultSet.getLong("idUser")).add(userInRoom);
                }
            }
            return concurrentHashMap;
        }catch (SQLException e){
            LOGGER.error(e);
            throw new ResponceExceptionImpl( TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }

    @Override
    public List<UserInRoom> getById(Long id) throws ResponceExceptionImpl {
        Connection connection = ConnectionMySQL.getConnectDB().connection;
        try(Statement query = connection.createStatement()) {
            ResultSet resultSet = query.executeQuery(
                    "SELECT `idUser`, `idRoom` " +
                            "FROM `userInRoom`" +
                            "WHERE idUser = " + id);

            List<UserInRoom> list = new LinkedList<>();

            while (resultSet.next()) {
                list.add(new UserInRoom(resultSet.getLong("idUser"),
                        resultSet.getLong("idRoom")));
            }

            return list;
        }catch (SQLException e){
            LOGGER.error(e);
            throw new ResponceExceptionImpl( TypeException.SQL_EXCEPTION, e.getMessage());
        }
    }

    @Override
    public List<UserInRoom> setNewField(List<UserInRoom> object){
        return null;
    }

    @Override
    public void setUpdateById(long id, List<UserInRoom> object){

    }

    @Override
    public void importToDB(List<List<UserInRoom>> list) throws ResponceExceptionImpl {

        Connection connection = ConnectionMySQL.getConnectDB().connection;

        String queryLessons = "INSERT INTO `userInRoom` (`idUser`, `idRoom`) " +
                "VALUES (?, ?);";

        try {


            try {
                PreparedStatement pst = connection.prepareStatement(queryLessons);

                list.forEach(l -> l.forEach(userInRoom -> {
                    try {
                        pst.setLong(1, userInRoom.getIdroom());
                        pst.setLong(2, userInRoom.getIduser());

                        pst.addBatch();

                    } catch (SQLException e) {
                        LOGGER.debug(e);
                    }
                }));

                int[] count = pst.executeBatch();

            } catch (BatchUpdateException ex) {
                LOGGER.debug(ex.getMessage());
                Reporter.importReport(ex, tableName);
            }
        }catch (SQLException e){
            LOGGER.error(e);
            throw new ResponceExceptionImpl( TypeException.SQL_EXCEPTION, e.getMessage());
        }

    }
}
