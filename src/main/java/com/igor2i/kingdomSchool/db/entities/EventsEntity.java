package com.igor2i.kingdomSchool.db.entities;

import javax.persistence.*;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "events", schema = "kingdom_school", catalog = "")
public class EventsEntity {
    private long id;
    private long idUserTo;
    private String text;
    private String head;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "idUserTo")
    public long getIdUserTo() {
        return idUserTo;
    }

    public void setIdUserTo(long idUserTo) {
        this.idUserTo = idUserTo;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "head")
    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventsEntity that = (EventsEntity) o;

        if (id != that.id) return false;
        if (idUserTo != that.idUserTo) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (head != null ? !head.equals(that.head) : that.head != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (idUserTo ^ (idUserTo >>> 32));
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (head != null ? head.hashCode() : 0);
        return result;
    }
}
