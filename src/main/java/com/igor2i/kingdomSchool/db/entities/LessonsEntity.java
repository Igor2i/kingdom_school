package com.igor2i.kingdomSchool.db.entities;

import com.igor2i.kingdomSchool.db.modelsBO.Lessons;

import javax.persistence.*;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "lessons", schema = "kingdom_school", catalog = "")
public class LessonsEntity {
    private long id;
    private boolean del;
    private long owner;
    private String type;
    private String linkObject;
    private int rating;
    private int experience;
    private String header;
    private String shortDescription;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "del")
    public boolean getDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    @Basic
    @Column(name = "owner")
    public long getOwner() {
        return owner;
    }

    public void setOwner(long owner) {
        this.owner = owner;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "linkObject")
    public String getLinkObject() {
        return linkObject;
    }

    public void setLinkObject(String linkObject) {
        this.linkObject = linkObject;
    }

    @Basic
    @Column(name = "rating")
    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Basic
    @Column(name = "experience")
    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    @Basic
    @Column(name = "header")
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    @Basic
    @Column(name = "shortDescription")
    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Lessons convertToBO(){
        Lessons lessons = new Lessons();

        lessons.setId(this.getId());
        lessons.setDel(this.getDel());
        lessons.setExperience(this.getExperience());
        lessons.setHeader(this.getHeader());
        lessons.setLinkObject(this.getLinkObject());
        lessons.setOwner(this.getOwner());
        lessons.setRating(this.getRating());
        lessons.setShortDescription(this.getShortDescription());
        lessons.setType(this.getType());

        return lessons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LessonsEntity that = (LessonsEntity) o;

        if (id != that.id) return false;
        if (del != that.del) return false;
        if (owner != that.owner) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (rating != that.rating) return false;
        if (experience != that.experience) return false;
        if (linkObject != null ? !linkObject.equals(that.linkObject) : that.linkObject != null) return false;
        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (shortDescription != null ? !shortDescription.equals(that.shortDescription) : that.shortDescription != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + Integer.valueOf(String.valueOf(del));
        result = 31 * result + (int) (owner ^ (owner >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (linkObject != null ? linkObject.hashCode() : 0);
        result = 31 * result + rating;
        result = 31 * result + experience;
        result = 31 * result + (header != null ? header.hashCode() : 0);
        result = 31 * result + (shortDescription != null ? shortDescription.hashCode() : 0);
        return result;
    }
}
