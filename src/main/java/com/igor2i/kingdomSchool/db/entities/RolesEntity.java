package com.igor2i.kingdomSchool.db.entities;

import javax.persistence.*;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "roles", schema = "kingdom_school", catalog = "")
public class RolesEntity {
    private int id;
    private String nameRole;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nameRole")
    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RolesEntity that = (RolesEntity) o;

        if (id != that.id) return false;
        if (nameRole != null ? !nameRole.equals(that.nameRole) : that.nameRole != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nameRole != null ? nameRole.hashCode() : 0);
        return result;
    }
}
