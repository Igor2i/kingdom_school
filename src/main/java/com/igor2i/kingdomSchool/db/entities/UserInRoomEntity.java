package com.igor2i.kingdomSchool.db.entities;


import javax.persistence.*;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "userInRoom", schema = "kingdom_school", catalog = "")
public class UserInRoomEntity {
    private long idUser;
    private long idRoom;

    @Id
    @Column(name = "idUser")
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "idRoom")
    public long getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(long idRoom) {
        this.idRoom = idRoom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserInRoomEntity that = (UserInRoomEntity) o;

        if (idUser != that.idUser) return false;
        if (idRoom != that.idRoom) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idUser ^ (idUser >>> 32));
        result = 31 * result + (int) (idRoom ^ (idRoom >>> 32));
        return result;
    }
}
