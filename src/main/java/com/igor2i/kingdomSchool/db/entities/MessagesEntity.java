package com.igor2i.kingdomSchool.db.entities;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "messages", schema = "kingdom_school", catalog = "")
public class MessagesEntity {
    private long id;
    private long room;
    private long idUserFrom;
    private long idUserTo;
    private String text;
    private Timestamp timeStamp;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "room")
    public long getRoom() {
        return room;
    }

    public void setRoom(long room) {
        this.room = room;
    }

    @Basic
    @Column(name = "idUserFrom")
    public long getIdUserFrom() {
        return idUserFrom;
    }

    public void setIdUserFrom(long idUserFrom) {
        this.idUserFrom = idUserFrom;
    }

    @Basic
    @Column(name = "idUserTo")
    public long getIdUserTo() {
        return idUserTo;
    }

    public void setIdUserTo(long idUserTo) {
        this.idUserTo = idUserTo;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "timeStamp")
    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessagesEntity that = (MessagesEntity) o;

        if (id != that.id) return false;
        if (room != that.room) return false;
        if (idUserFrom != that.idUserFrom) return false;
        if (idUserTo != that.idUserTo) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (timeStamp != null ? !timeStamp.equals(that.timeStamp) : that.timeStamp != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (room ^ (room >>> 32));
        result = 31 * result + (int) (idUserFrom ^ (idUserFrom >>> 32));
        result = 31 * result + (int) (idUserTo ^ (idUserTo >>> 32));
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (timeStamp != null ? timeStamp.hashCode() : 0);
        return result;
    }
}
