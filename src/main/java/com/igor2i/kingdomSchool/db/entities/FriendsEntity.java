package com.igor2i.kingdomSchool.db.entities;

import javax.persistence.*;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "friends", schema = "kingdom_school", catalog = "")
public class FriendsEntity {
    private long id;
    private long idUser;
    private long idUserFrieand;
    private int confirmed;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "idUser")
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "idUserFrieand")
    public long getIdUserFrieand() {
        return idUserFrieand;
    }

    public void setIdUserFrieand(long idUserFrieand) {
        this.idUserFrieand = idUserFrieand;
    }

    @Basic
    @Column(name = "confirmed")
    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FriendsEntity that = (FriendsEntity) o;

        if (id != that.id) return false;
        if (idUser != that.idUser) return false;
        if (idUserFrieand != that.idUserFrieand) return false;
        if (confirmed != that.confirmed) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (idUser ^ (idUser >>> 32));
        result = 31 * result + (int) (idUserFrieand ^ (idUserFrieand >>> 32));
        result = 31 * result + confirmed;
        return result;
    }
}
