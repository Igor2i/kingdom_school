package com.igor2i.kingdomSchool.db.entities;

import com.igor2i.kingdomSchool.db.modelsBO.TeacherResume;

import javax.persistence.*;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "teacherResume", schema = "kingdom_school", catalog = "")
public class TeacherResumeEntity {
    private long idUser;
    private String header;
    private String body;

    @Id
    @Column(name = "idUser")
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "header")
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    @Basic
    @Column(name = "body")
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public TeacherResume convertToBO(){
        TeacherResume teacherResume = new TeacherResume();

        teacherResume.setIduser(this.getIdUser());
        teacherResume.setHeader(this.getHeader());
        teacherResume.setBody(this.getBody());

        return teacherResume;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TeacherResumeEntity that = (TeacherResumeEntity) o;

        if (idUser != that.idUser) return false;
        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (body != null ? !body.equals(that.body) : that.body != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idUser ^ (idUser >>> 32));
        result = 31 * result + (header != null ? header.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        return result;
    }
}
