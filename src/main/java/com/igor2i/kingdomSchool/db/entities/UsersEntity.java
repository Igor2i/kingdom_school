package com.igor2i.kingdomSchool.db.entities;

import com.igor2i.kingdomSchool.db.modelsBO.User;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "users", schema = "kingdom_school", catalog = "", uniqueConstraints = {
        @UniqueConstraint(columnNames = "login"),
        @UniqueConstraint(columnNames = "userKey")
})
public class UsersEntity {
    private long id;
    private String passwd;
    private String email;
    private String userKey;
    private boolean del;
    private int role;
    private String login;
    private boolean blocked;
    private String firstName;
    private String lastName;
    private Date lastAccess;
    private UsersInfoEntity usersInfoEntity;

    public UsersEntity() {
    }

    public UsersEntity(long id, String passwd, String email, String userKey, boolean del, int role, String login, boolean blocked, String firstName, String lastName, Date lastAccess, UsersInfoEntity usersInfoEntity) {
        this.id = id;
        this.passwd = passwd;
        this.email = email;
        this.userKey = userKey;
        this.del = del;
        this.role = role;
        this.login = login;
        this.blocked = blocked;
        this.firstName = firstName;
        this.lastName = lastName;
        this.lastAccess = lastAccess;
        this.usersInfoEntity = usersInfoEntity;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "passwd")
    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "userKey")
    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    @Basic
    @Column(name = "del")
    public boolean getDel() {
        return del;
    }

    public void setDel(boolean del) {
        this.del = del;
    }

    @Basic
    @Column(name = "role")
    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Basic
    @Column(name = "login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "blocked")
    public boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    @Basic
    @Column(name = "firstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "lastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "lastAccess")
    public Date getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "usersEntity", cascade = CascadeType.ALL )
    public UsersInfoEntity getUsersInfoEntity() {
        return usersInfoEntity;
    }

    public void setUsersInfoEntity(UsersInfoEntity usersInfoEntity) {
        this.usersInfoEntity = usersInfoEntity;
    }

    public User convertToBO(){
        User user = new User();

        user.setId(this.getId());
        user.setLogin(this.getLogin());
        user.setPasswd(this.getPasswd());
        user.setEmail(this.getEmail());
        user.setUserKey(this.getUserKey());
        user.setDel(this.getDel());
        user.setBlocked(this.getBlocked());
        user.setRole(this.getRole());
        user.setFirstName(this.getFirstName());
        user.setLastName(this.getLastName());
        user.setLastAccess(this.getLastAccess());
        user.setMale(this.getUsersInfoEntity().getMale());
        user.setDob(this.getUsersInfoEntity().getDob());
        user.setSocial(this.getUsersInfoEntity().getSocial());

        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersEntity that = (UsersEntity) o;

        if (id != that.id) return false;
        if (del != that.del) return false;
        if (role != that.role) return false;
        if (passwd != null ? !passwd.equals(that.passwd) : that.passwd != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (userKey != null ? !userKey.equals(that.userKey) : that.userKey != null) return false;
        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (lastAccess != null ? !lastAccess.equals(that.lastAccess) : that.lastAccess != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (passwd != null ? passwd.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (userKey != null ? userKey.hashCode() : 0);
        result = 31 * result + Integer.valueOf(String.valueOf(del));
        result = 31 * result + (int) (role ^ (role >>> 32));
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + Integer.valueOf(String.valueOf(blocked));
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (lastAccess != null ? lastAccess.hashCode() : 0);
        return result;
    }
}
