package com.igor2i.kingdomSchool.db.entities;

import com.igor2i.kingdomSchool.db.modelsBO.UserExperience;

import javax.persistence.*;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "usersExperience", schema = "kingdom_school", catalog = "")
public class UsersExperienceEntity {
    private long idUser;
    private int experience;

    @Id
    @Column(name = "idUser")
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "experience")
    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public UserExperience convertToBO(){
        UserExperience userExperience = new UserExperience();

        userExperience.setIdUser(this.getIdUser());
        userExperience.setExperience(this.getExperience());

        return userExperience;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersExperienceEntity that = (UsersExperienceEntity) o;

        if (idUser != that.idUser) return false;
        if (experience != that.experience) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idUser ^ (idUser >>> 32));
        result = 31 * result + experience;
        return result;
    }
}
