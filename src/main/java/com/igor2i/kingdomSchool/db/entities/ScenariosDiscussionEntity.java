package com.igor2i.kingdomSchool.db.entities;

import javax.persistence.*;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "scenariosDiscussion", schema = "kingdom_school", catalog = "")
public class ScenariosDiscussionEntity {
    private long id;
    private int del;
    private String linkObject;
    private int rating;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "del")
    public int getDel() {
        return del;
    }

    public void setDel(int del) {
        this.del = del;
    }

    @Basic
    @Column(name = "linkObject")
    public String getLinkObject() {
        return linkObject;
    }

    public void setLinkObject(String linkObject) {
        this.linkObject = linkObject;
    }

    @Basic
    @Column(name = "rating")
    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScenariosDiscussionEntity that = (ScenariosDiscussionEntity) o;

        if (id != that.id) return false;
        if (del != that.del) return false;
        if (rating != that.rating) return false;
        if (linkObject != null ? !linkObject.equals(that.linkObject) : that.linkObject != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + del;
        result = 31 * result + (linkObject != null ? linkObject.hashCode() : 0);
        result = 31 * result + rating;
        return result;
    }
}
