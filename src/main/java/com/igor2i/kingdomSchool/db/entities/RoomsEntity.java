package com.igor2i.kingdomSchool.db.entities;

import javax.persistence.*;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "rooms", schema = "kingdom_school", catalog = "")
public class RoomsEntity {
    private int id;
    private int del;
    private long owner;
    private int access;
    private String link;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "del")
    public int getDel() {
        return del;
    }

    public void setDel(int del) {
        this.del = del;
    }

    @Basic
    @Column(name = "owner")
    public long getOwner() {
        return owner;
    }

    public void setOwner(long owner) {
        this.owner = owner;
    }

    @Basic
    @Column(name = "access")
    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    @Basic
    @Column(name = "link")
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomsEntity that = (RoomsEntity) o;

        if (id != that.id) return false;
        if (del != that.del) return false;
        if (owner != that.owner) return false;
        if (access != that.access) return false;
        if (link != null ? !link.equals(that.link) : that.link != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + del;
        result = 31 * result + (int) (owner ^ (owner >>> 32));
        result = 31 * result + access;
        result = 31 * result + (link != null ? link.hashCode() : 0);
        return result;
    }
}
