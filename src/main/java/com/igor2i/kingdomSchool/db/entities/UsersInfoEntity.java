package com.igor2i.kingdomSchool.db.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by igor2i on 04.03.17.
 */
@Entity
@Table(name = "usersInfo", schema = "kingdom_school", catalog = "")
public class UsersInfoEntity {
    private long idUser;
    private Date dob;
    private boolean male;
    private String social;
    private UsersEntity usersEntity;

    public UsersInfoEntity() {
    }

    public UsersInfoEntity(long idUser, Date dob, boolean male, String social, UsersEntity usersEntity) {
        this.idUser = idUser;
        this.dob = dob;
        this.male = male;
        this.social = social;
        this.usersEntity = usersEntity;
    }

    @Id
    @GeneratedValue(generator = "generator")
    @GenericGenerator(name = "generator", strategy = "foreign",
            parameters = @Parameter(name = "property", value = "usersEntity"))
    @Column(name = "idUser", unique = true, nullable = false)
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "dob")
    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @Basic
    @Column(name = "male")
    public boolean getMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    @Basic
    @Column(name = "social")
    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public UsersEntity getUsersEntity() {
        return usersEntity;
    }

    public void setUsersEntity(UsersEntity usersEntity) {
        this.usersEntity = usersEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersInfoEntity that = (UsersInfoEntity) o;

        if (idUser != that.idUser) return false;
        if (male != that.male) return false;
        if (dob != null ? !dob.equals(that.dob) : that.dob != null) return false;
        if (social != null ? !social.equals(that.social) : that.social != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idUser ^ (idUser >>> 32));
        result = 31 * result + (dob != null ? dob.hashCode() : 0);
        result = 31 * result + Integer.valueOf(String.valueOf(male));
        result = 31 * result + (social != null ? social.hashCode() : 0);
        return result;
    }
}
