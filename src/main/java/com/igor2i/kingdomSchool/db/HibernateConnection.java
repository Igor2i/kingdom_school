package com.igor2i.kingdomSchool.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;

/**
 * Created by igor2i on 05.03.17.
 */
@Component
class HibernateConnection {

    private static final Logger LOGGER = LogManager.getLogger(HibernateConnection.class);


    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory(){
        try {
            return new Configuration().configure().buildSessionFactory();
        } catch (Throwable e) {
            LOGGER.error(e);
            throw new ExceptionInInitializerError(e);
        }
    }

    protected static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    protected static void shutdown() {
        getSessionFactory().close();
    }

}
