package com.igor2i.kingdomSchool;


import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.exporter.ExportFromDBToXML;
import com.igor2i.kingdomSchool.importer.ImportFromXMLToDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

/**
 * Created by igor2i on 18.02.17.
 */
public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) {


        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                ExportFromDBToXML exportFromDBToXML = new ExportFromDBToXML();
                try {
                    exportFromDBToXML.exportUsers(new File("usersXML"));
                } catch (ResponceExceptionImpl e) {
                    LOGGER.error(e);
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                ExportFromDBToXML exportFromDBToXML = new ExportFromDBToXML();
                try {
                    exportFromDBToXML.exportLessons(new File("lessonsXML"));
                } catch (ResponceExceptionImpl e) {
                    LOGGER.error(e);
                }
            }
        });

        thread1.start();
        thread2.start();


        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    thread1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ImportFromXMLToDB importFromXMLToDB = new ImportFromXMLToDB();
                try {
                    importFromXMLToDB.importUsers(new File("usersXML"));
                } catch (ResponceExceptionImpl e) {
                    LOGGER.error(e);
                }
            }
        });

        Thread thread4 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    thread2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ImportFromXMLToDB importFromXMLToDB = new ImportFromXMLToDB();
                try {
                    importFromXMLToDB.importLessons(new File("lessonsXML"));
                } catch (ResponceExceptionImpl e) {
                    LOGGER.error(e);
                }
            }
        });

        thread3.start();
        thread4.start();
        

    }

}
