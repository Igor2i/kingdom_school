package com.igor2i.kingdomSchool.parser.containers;

import com.igor2i.kingdomSchool.db.modelsBO.Lessons;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by igor2i on 19.02.17.
 */
@XmlRootElement(name = "lessons")
@XmlAccessorType(XmlAccessType.FIELD)
public class LessonsContainer {

    @XmlAttribute
    private long serialVersionUID = Lessons.getSerialVersionUID();

    @XmlElement(name = "lesson")
    private List<Lessons> lessonsList = null;

    public List<Lessons> getLessonsList() {
        return lessonsList;
    }

    public void setLessonsList(List<Lessons> lessonsList) {
        this.lessonsList = lessonsList;
    }
}
