package com.igor2i.kingdomSchool.parser.containers;

import com.igor2i.kingdomSchool.db.modelsBO.User;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by igor2i on 19.02.17.
 */
@XmlRootElement(name = "users")
@XmlAccessorType(XmlAccessType.FIELD)
public class UsersContainer {

    @XmlAttribute
    private long serialVersionUID = User.getSerialVersionUID();

    @XmlElement(name = "user")
    private List<User> userList = null;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
