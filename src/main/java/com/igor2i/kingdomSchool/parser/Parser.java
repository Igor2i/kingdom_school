package com.igor2i.kingdomSchool.parser;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;

import java.io.File;

/**
 * Created by igor2i on 19.02.17.
 */
public interface Parser {

    Object getObject(File file, Class c) throws ResponceExceptionImpl;

    void saveObject(File file, Object o) throws ResponceExceptionImpl;

}
