package com.igor2i.kingdomSchool.parser.impl;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.parser.Parser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.*;
import java.io.File;

/**
 * Created by igor2i on 16.02.17.
 */
public class JaxbParser implements Parser {

    private static final Logger LOGGER = LogManager.getLogger(JaxbParser.class);


    @Override
    public Object getObject(File file, Class c) throws ResponceExceptionImpl {
        JAXBContext context = null;
        try {
            context = JAXBContext.newInstance(c);

            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object object = unmarshaller.unmarshal(file);

            return object;
        } catch (JAXBException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl( TypeException.PARSE_EXCEPTION, e.getMessage());
        }
    }

    @Override
    public void saveObject(File file, Object o) throws ResponceExceptionImpl {
        try {
            JAXBContext context = JAXBContext.newInstance(o.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(o, file);
        } catch (JAXBException e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl( TypeException.PARSE_EXCEPTION, e.getMessage());
        }
    }
}
