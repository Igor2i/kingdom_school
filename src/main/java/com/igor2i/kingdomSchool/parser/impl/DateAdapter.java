package com.igor2i.kingdomSchool.parser.impl;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.UsersInDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by igor2i on 16.02.17.
 */
@Deprecated
public class DateAdapter extends XmlAdapter<String, Date> {

    private static final Logger LOGGER = LogManager.getLogger(UsersInDB.class);


    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public String marshal(Date v){
        synchronized (dateFormat) {
            return dateFormat.format(v);
        }
    }

    @Override
    public Date unmarshal(String v) throws ResponceExceptionImpl {
        synchronized (dateFormat) {
            try {
                return dateFormat.parse(v);
            } catch (ParseException e) {
                LOGGER.error(e);
                throw new ResponceExceptionImpl(TypeException.EXCEPTION, e.getMessage());
            }
        }
    }

}
