package com.igor2i.kingdomSchool.exporter;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.LessonsInDB;
import com.igor2i.kingdomSchool.db.UsersInDB;
import com.igor2i.kingdomSchool.db.modelsBO.Lessons;
import com.igor2i.kingdomSchool.db.modelsBO.User;
import com.igor2i.kingdomSchool.parser.Parser;
import com.igor2i.kingdomSchool.parser.containers.LessonsContainer;
import com.igor2i.kingdomSchool.parser.containers.UsersContainer;
import com.igor2i.kingdomSchool.parser.impl.JaxbParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 20.02.17.
 */
public class ExportFromDBToXML {

    private static final Logger LOGGER = LogManager.getLogger(ExportFromDBToXML.class);

    public void exportUsers(File file) throws ResponceExceptionImpl {

        try {
            UsersInDB usersInDB = new UsersInDB();
            Parser parser = new JaxbParser();
            ConcurrentHashMap<Long, User> usersHM = usersInDB.getAll();

            usersHM.forEach((id, user) -> LOGGER.debug(user.toString()));

            UsersContainer usersContainer = new UsersContainer();
            usersContainer.setUserList(new ArrayList<>(usersHM.values()));

            parser.saveObject(file, usersContainer);

        }catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl( TypeException.PARSE_EXCEPTION, e.getMessage());
        }

    }

    public void exportLessons(File file) throws ResponceExceptionImpl {

        try {
            LessonsInDB lessonsInDB = new LessonsInDB();
            Parser parser = new JaxbParser();
            ConcurrentHashMap<Long, Lessons> lessonsCHM = lessonsInDB.getAll();

            lessonsCHM.forEach((id, user) -> LOGGER.debug(user.toString()));

            LessonsContainer lessonsContainer = new LessonsContainer();
            lessonsContainer.setLessonsList(new ArrayList<>(lessonsCHM.values()));

            parser.saveObject(file, lessonsContainer);

        }catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl( TypeException.PARSE_EXCEPTION, e.getMessage());
        }

    }

}
