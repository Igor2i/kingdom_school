package com.igor2i.kingdomSchool.controller;

import com.igor2i.kingdomSchool.controller.listeners.LessonsMemoryContextListener;
import com.igor2i.kingdomSchool.controller.listeners.UserExpMemoryContextListener;
import com.igor2i.kingdomSchool.db.modelsBO.Lessons;
import com.igor2i.kingdomSchool.db.modelsBO.User;
import com.igor2i.kingdomSchool.services.AuthService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by igor2i on 26.02.17.
 */
@WebServlet("/lesson")
@Component
public class LessonServlet extends HttpServlet {


    private static final Logger LOGGER = LogManager.getLogger(LessonServlet.class);

    @Autowired
    private AuthService authService;
    @Autowired
    private UserExpMemoryContextListener userExpMemoryContextListener;
    @Autowired
    private LessonsMemoryContextListener lessonsMemoryContextListener;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LOGGER.debug("getLesson " + req.getRequestURI());

        authService.addAttribAuthUser(req);

        String idStr = req.getParameter("id");

        if (idStr != null) {
            int idLesson = Integer.valueOf(idStr);

            Lessons lessons = lessonsMemoryContextListener.getLessonById(idLesson);

            if (lessons != null) {

                req.setAttribute("headerLesson", lessons.getHeader());

                //TODO Сделать обработку JSON объекта или NoSQL
                req.setAttribute("bodyLesson", lessons.getLinkObject());

                setReadyBlock(req);

                req.getRequestDispatcher("lesson.jsp").forward(req, resp);
                return;
            }
        }
        resp.sendRedirect("/");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //TODO доработать интерактивность
        LOGGER.debug("postLesson " + req.getRequestURI());

        String idStr = req.getParameter("id");
        if (idStr != null) {
            int idLesson = Integer.valueOf(idStr);

            Lessons lessons = lessonsMemoryContextListener.getLessonById(idLesson);

            if (lessons != null) {
                User user = authService.getAuthUser(req);
                if (user != null) {
                    userExpMemoryContextListener.addUserExp(user.getId(), lessons.getExperience());
                }
            }
        }
        resp.sendRedirect("/");
    }

    private void setReadyBlock(HttpServletRequest req) {

        StringBuilder htmlReadyButtom = new StringBuilder();
        htmlReadyButtom.append("<form action=\"/lesson?id=")
                .append(req.getParameter("id"))
                .append("\" method=\"post\">")
                .append("<input type=\"submit\" class=\"btn btn-success center-block\" value=\"Я всё понял\">")
                .append("</form>");
        req.setAttribute("readyBlock", htmlReadyButtom);

    }
}
