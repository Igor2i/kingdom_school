package com.igor2i.kingdomSchool.controller;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.controller.enums.RolesType;
import com.igor2i.kingdomSchool.controller.enums.TypeEvent;
import com.igor2i.kingdomSchool.controller.listeners.TeachersResumeMemoryContextListener;
import com.igor2i.kingdomSchool.controller.listeners.UserMemoryServletContextListener;
import com.igor2i.kingdomSchool.db.modelsBO.TeacherResume;
import com.igor2i.kingdomSchool.db.modelsBO.User;
import com.igor2i.kingdomSchool.services.AuthService;
import com.igor2i.kingdomSchool.services.EventService;
import com.igor2i.kingdomSchool.services.HTMLGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by igor2i on 02.03.17.
 */
@WebServlet("/profile-edit")
@Component
public class ProfileEditServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(ProfileEditServlet.class);

    @Autowired
    private AuthService authService;
    @Autowired
    private HTMLGenerator htmlGenerator;
    @Autowired
    private UserMemoryServletContextListener userMemoryServletContextListener;
    @Autowired
    private TeachersResumeMemoryContextListener teachersResumeMemoryContextListener;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LOGGER.debug("getEditProfile " + req.getRequestURI());

        authService.addAttribAuthUser(req);
        htmlGenerator.setHeadPage(req, "Редактирование профиля | KingDom School");
        htmlGenerator.setAboutModulePage(req);
        htmlGenerator.setFooterScriptsPage(req);
        htmlGenerator.setHeaderFirstMenuPage(req);
        try {
            authService.addAttribProfileUser(req);
        } catch (ResponceExceptionImpl e) {
            EventService.addAttribEvent(req, TypeEvent.ERROR_LOGIN_CONTAIN);
            req.getRequestDispatcher("/").forward(req, resp);
            return;
        }

        req.getRequestDispatcher("profileEdit.jsp").forward(req, resp);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LOGGER.debug("postEditProfile");

        User user = authService.getAuthUser(req);
        if (user != null) {


            req.setCharacterEncoding("UTF-8");

            //TODO сделать проверку входных данных

            String email = req.getParameter("email");
            String lastname = req.getParameter("lastname");
            String firstname = req.getParameter("firstname");
            String male = req.getParameter("male");
            String vklink = req.getParameter("vklink");
            boolean updateUser = false;

            if (email != null && !"".equals(email)) {
                user.setEmail(email);
                updateUser = true;
            }
            if (lastname != null && !"".equals(lastname)) {
                user.setLastName(lastname);
                updateUser = true;
            }
            if (firstname != null && !"".equals(firstname)) {
                user.setFirstName(firstname);
                updateUser = true;
            }
            if (male != null && (1 == Integer.valueOf(male) || 0 == Integer.valueOf(male))) {
                user.setMale(1 == Integer.valueOf(male));
                updateUser = true;
            }
            if (vklink != null && !"".equals(vklink)) {
                user.setSocial(vklink);
                updateUser = true;
            }


            boolean updateResume = false;
            TeacherResume teacherResume = new TeacherResume();
            if (RolesType.TEACHER.isRole(user.getRole()) || RolesType.UP_STUDENT.isRole(user.getRole())) {
                String resumeHead = req.getParameter("resumeHead");
                String resumeBody = req.getParameter("resumeBody");

                teacherResume.setIduser(user.getId());
                teacherResume.setLastName(user.getLastName());
                teacherResume.setFirstName(user.getFirstName());

                if (resumeHead != null && !"".equals(resumeHead)) {
                    teacherResume.setHeader(resumeHead);
                    updateResume = true;
                }
                if (resumeBody != null && !"".equals(resumeBody)) {
                    teacherResume.setBody(resumeBody);
                    updateResume = true;
                }

            }

            if(updateUser){
                try {
                    userMemoryServletContextListener.setUpdateUser(user);
                } catch (ResponceExceptionImpl e) {
                    EventService.addAttribEvent(req, TypeEvent.ERROR_UPDATE_USER);
                    req.getRequestDispatcher("profileEdit.jsp").forward(req, resp);
                    return;
                }
            }
            if(updateResume){
                try {
                    teachersResumeMemoryContextListener.setUpdateResume(teacherResume);
                } catch (ResponceExceptionImpl e) {
                    EventService.addAttribEvent(req, TypeEvent.ERROR_UPDATE_USER);
                    req.getRequestDispatcher("profileEdit.jsp").forward(req, resp);
                    return;
                }
            }

            EventService.addAttribEvent(req, TypeEvent.UPDATE_PROFILE_SUCCESS);
            req.getRequestDispatcher("/").forward(req, resp);

        } else {

            EventService.addAttribEvent(req, TypeEvent.ERROR_LOGIN_CONTAIN);
            req.getRequestDispatcher("/").forward(req, resp);
        }


    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }
}
