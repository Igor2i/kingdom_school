package com.igor2i.kingdomSchool.controller;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.controller.enums.TypeEvent;
import com.igor2i.kingdomSchool.controller.listeners.UserMemoryServletContextListener;
import com.igor2i.kingdomSchool.db.modelsBO.User;
import com.igor2i.kingdomSchool.services.AuthService;
import com.igor2i.kingdomSchool.services.EventService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by igor2i on 24.02.17.
 */
@WebServlet("/registration")
@Component
public class RegistrationServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(RegistrationServlet.class);

    private boolean fail = false;

    @Autowired
    private UserMemoryServletContextListener userMemoryServletContextListener;

    @Autowired
    private AuthService authService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LOGGER.debug("getRegist");

        authService.addAttribAuthUser(req);

        req.getRequestDispatcher("registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LOGGER.debug("postRegist");


        req.setCharacterEncoding("UTF-8");
        fail = false;
        //TODO сделать проверку входных данных

        String login = req.getParameter("loginReg");
        String passwd = req.getParameter("password");
        String email = req.getParameter("email");
        String lastname = req.getParameter("lastname");
        String firstname = req.getParameter("firstname");
        String dob = req.getParameter("dob");
        String male = req.getParameter("male");
        String vklink = req.getParameter("vklink");
        String techer = req.getParameter("techer");

        verificationUserInfo(req, login, passwd, email);
        Date dateDob = getDateType(req, dob);

        if (fail) {

            req.getRequestDispatcher("registration.jsp").forward(req, resp);

        } else {

            User user = new User();
            user.setLogin(login);
            user.setPasswd(cryptStringToSHA256(passwd));
            user.setEmail(email);
            user.setLastName(lastname);
            user.setFirstName(firstname);
            user.setDob(dateDob);
            user.setMale("1".equals(male));
            user.setSocial(vklink);

            user.setUserKey(genUserKey(login, passwd));
            user.setRole("1".equals(techer) ? 4 : 5);
            user.setDel(false);
            user.setBlocked(false);

            try {
                userMemoryServletContextListener.registNewUser(user);
            } catch (ResponceExceptionImpl e) {
                EventService.addAttribEvent(req, TypeEvent.ERROR_CREATE_USER);
                req.getRequestDispatcher("/").forward(req, resp);
                return;
            }
            EventService.addAttribEvent(req, TypeEvent.EVENT_HELLO_NEW_USER);
            req.getRequestDispatcher("/").forward(req, resp);
        }
    }


    private String cryptStringToSHA256(String text) {

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
            String servernayaSol = "My dobavim nemnogo soli dlya etogo POLZOVATELYZ";
            md.update((text + servernayaSol).getBytes("UTF-8"));
            byte[] cryptBytes = md.digest();

            return String.format("%064x", new java.math.BigInteger(1, cryptBytes));

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            LOGGER.error("Fail crypt " + e.getMessage());
            return text;
        }
    }


    private void verificationUserInfo(HttpServletRequest req, String login, String passwd, String email) {
        //TODO переделать через EventService
        if ("".equals(login)) {
            EventService.addAttribEvent(req, TypeEvent.ERROR_LOGIN);
            fail = true;
        } else if (userMemoryServletContextListener.isUserLogin(login)) {
            EventService.addAttribEvent(req, TypeEvent.ERROR_LOGIN_CONTAIN);
            fail = true;
        } else if ("".equals(passwd)) {
            EventService.addAttribEvent(req, TypeEvent.ERROR_PASSWORD);
            fail = true;
        } else if ("".equals(email)) {
            EventService.addAttribEvent(req, TypeEvent.ERROR_EMAIL);
            fail = true;
        }
    }


    private Date getDateType(HttpServletRequest req, String dob) {
        Date dateDob = null;
        try {
            dateDob = new SimpleDateFormat("yyyy-MM-dd").parse(dob);
        } catch (ParseException e) {
            //TODO переделать через EventService
            EventService.addAttribEvent(req, TypeEvent.ERROR_DATE_TYPE);
            fail = true;
        }
        return dateDob;
    }


    private String genUserKey(String login, String pass) {
        Random random = new Random();
        String sol = "Hell0 My New Fr1enD";

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update((login + random.nextInt(1000000) + pass + sol).getBytes("UTF-8"));
            byte[] cryptBytes = md.digest();

            return String.format("%032x", new java.math.BigInteger(1, cryptBytes));

        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            LOGGER.error(e.getMessage());
        }

        return login + random.nextInt(100000);
    }

}
