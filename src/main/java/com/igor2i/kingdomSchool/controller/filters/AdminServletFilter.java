package com.igor2i.kingdomSchool.controller.filters;

import com.igor2i.kingdomSchool.controller.enums.RolesType;
import com.igor2i.kingdomSchool.controller.enums.TypeEvent;
import com.igor2i.kingdomSchool.controller.listeners.UserMemoryServletContextListener;
import com.igor2i.kingdomSchool.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by igor2i on 25.02.17.
 */
@WebFilter(
        filterName = "AdminFilter",
        urlPatterns = {"/admin/*"})
@Component
public class AdminServletFilter implements Filter {

    @Autowired
    private UserMemoryServletContextListener userMemoryServletContextListener;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        WebApplicationContextUtils
                .getRequiredWebApplicationContext(filterConfig.getServletContext())
                .getAutowireCapableBeanFactory()
                .autowireBean(this);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        String userKey = (String) httpServletRequest.getSession().getAttribute("userKey");

        if (userKey != null) {
            if (RolesType.ADMIN.isRole(userMemoryServletContextListener.getUserByUserKey(userKey).getRole())) {
                chain.doFilter(request, response);
                return;
            }
        }

        EventService.addAttribEvent(httpServletRequest, TypeEvent.FOLDER_DENIED);
        httpServletRequest.getRequestDispatcher("/").forward(request, response);

    }

    @Override
    public void destroy() {

    }

}
