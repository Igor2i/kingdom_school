package com.igor2i.kingdomSchool.controller.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by igor2i on 02.03.17.
 */

@WebFilter(
        filterName = "ServerFatalErrorAllFilter",
        urlPatterns = {""})
public class ServerFatalErrorAllFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(ServerFatalErrorAllFilter.class);


    private static boolean serverGoodHealth = true;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        WebApplicationContextUtils
                .getRequiredWebApplicationContext(filterConfig.getServletContext())
                .getAutowireCapableBeanFactory()
                .autowireBean(this);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (serverGoodHealth) {
            chain.doFilter(request, response);

        } else {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            //TODO errorPage
            httpServletRequest.getRequestDispatcher("error.jsp").forward(request, response);
        }


    }

    @Override
    public void destroy() {

    }

    public static boolean isServerGoodHealth() {
        return serverGoodHealth;
    }

    public static void setServerGoodHealth(boolean serverGoodHealth) {
        ServerFatalErrorAllFilter.serverGoodHealth = serverGoodHealth;
        LOGGER.warn("Server set BadHealth!");
    }
}
