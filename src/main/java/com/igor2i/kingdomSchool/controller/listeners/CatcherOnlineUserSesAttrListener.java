package com.igor2i.kingdomSchool.controller.listeners;

import com.igor2i.kingdomSchool.controller.enums.RolesType;
import com.igor2i.kingdomSchool.db.modelsBO.User;
import com.igor2i.kingdomSchool.services.EmailEventService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 23.02.17.
 */
@WebListener
public class CatcherOnlineUserSesAttrListener implements HttpSessionAttributeListener {

    private static final Logger LOGGER = LogManager.getLogger(CatcherOnlineUserSesAttrListener.class);

    private static ConcurrentHashMap<Long, User> onlineUsers = new ConcurrentHashMap<>(64);

    public static ConcurrentHashMap<Long, User> getUsersOnline(){
        return onlineUsers;
    }

    public static boolean isEventLoginForAdmins = true;

    @Autowired
    private EmailEventService emailEventService;

    private User user = null;

    private boolean init = false;

    @Autowired
    private UserMemoryServletContextListener userMemoryServletContextListener;

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {

        if(!init){
            init(event);
        }

        if("userKey".equals(event.getName()) ){
            user = userMemoryServletContextListener.getUserByUserKey((String) event.getValue());
            onlineUsers.put(user.getId(), user);
            LOGGER.debug( "Set session user " + user.getLogin());
            if( RolesType.ADMIN.isRole(user.getRole()) &&
                    isEventLoginForAdmins) {
                eventLoginForAdmins(user.getLogin());
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {

        if("userKey".equals(event.getName()) ){
            LOGGER.debug( "Logout session user " + user.getLogin());
            onlineUsers.remove(user.getId());
            event.getSession().invalidate();
        }

    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {

    }


    private void init(HttpSessionBindingEvent event){
        WebApplicationContextUtils
                .getRequiredWebApplicationContext(event.getSession().getServletContext())
                .getAutowireCapableBeanFactory()
                .autowireBean(this);
        init = true;

    }

    private void eventLoginForAdmins(String name){
        List<String> emailsAdmin = userMemoryServletContextListener.getAdminsEmail();

        for(String emailTo : emailsAdmin){
            emailEventService.eventLogin(emailTo, name);
        }

    }
}
