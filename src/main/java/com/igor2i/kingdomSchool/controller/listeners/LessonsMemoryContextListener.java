package com.igor2i.kingdomSchool.controller.listeners;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.controller.filters.ServerFatalErrorAllFilter;
import com.igor2i.kingdomSchool.db.modelsBO.Lessons;
import com.igor2i.kingdomSchool.services.LessonsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 26.02.17.
 */
@WebListener
@Service
public class LessonsMemoryContextListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(LessonsMemoryContextListener.class);

    @Autowired
    private LessonsService lessonsService;

    private static ConcurrentHashMap<Long, Lessons> lessons;


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOGGER.debug("init LessonsMemoryContextListener");

        WebApplicationContextUtils
                .getRequiredWebApplicationContext(sce.getServletContext())
                .getAutowireCapableBeanFactory()
                .autowireBean(this);

        try {
            lessons = lessonsService.getAll();
        } catch (ResponceExceptionImpl e) {
            LOGGER.fatal("Failed sql connection " + e);
            contextDestroyed(sce);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOGGER.debug("destroy UserDBServletContextListener");
        ServerFatalErrorAllFilter.setServerGoodHealth(false);
    }

    public ConcurrentHashMap<Long, Lessons> getAllLessons(){
        return lessons;
    }

    public Lessons getLessonById(long id){
        return lessons.get(id);
    }

    public ArrayList<Map.Entry<Integer, Lessons>> getSortByRatingLesson() {

        ArrayList<Map.Entry<Integer, Lessons>> sortedLessonsByMaxRating = new ArrayList<>(lessons.size());

        lessons.values().forEach(l -> sortedLessonsByMaxRating.add(new Map.Entry<Integer, Lessons>() {
            @Override
            public Integer getKey() {
                return l.getRating();
            }

            @Override
            public Lessons getValue() {
                return l;
            }

            @Override
            public Lessons setValue(Lessons value) {
                return value;
            }
        }));

        sortedLessonsByMaxRating.sort(Collections.reverseOrder(Comparator.comparingInt(Map.Entry::getKey)));

        return sortedLessonsByMaxRating;

    }
}
