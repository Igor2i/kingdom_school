package com.igor2i.kingdomSchool.controller.listeners;

import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.controller.enums.RolesType;
import com.igor2i.kingdomSchool.controller.filters.ServerFatalErrorAllFilter;
import com.igor2i.kingdomSchool.db.modelsBO.User;
import com.igor2i.kingdomSchool.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 23.02.17.
 */
@WebListener
@Service
public class UserMemoryServletContextListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(UserMemoryServletContextListener.class);

    @Autowired
    private UserService userService;
//    @Autowired
//    private TeachersResumeMemoryContextListener teachersResumeMemoryContextListener;

    private static ConcurrentHashMap<Long, User> users;

    private static ConcurrentHashMap<String, User> usersKeyAndUser = new ConcurrentHashMap<>(128);

    private static ConcurrentHashMap<String, User> usersLoginAndUser = new ConcurrentHashMap<>(128);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOGGER.debug("init UserDBServletContextListener");

        WebApplicationContextUtils
                .getRequiredWebApplicationContext(sce.getServletContext())
                .getAutowireCapableBeanFactory()
                .autowireBean(this);

        try {
            users = userService.getAll();
            users.values().forEach(user -> usersKeyAndUser.put(user.getUserKey(), user));
            users.values().forEach(user -> usersLoginAndUser.put(user.getLogin(), user));

        } catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
            contextDestroyed(sce);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOGGER.debug("destroy UserDBServletContextListener");
        ServerFatalErrorAllFilter.setServerGoodHealth(false);
    }


    public String getUserKey(String login, String passwd) {
        return usersLoginAndUser.containsKey(login) && usersLoginAndUser.get(login).getPasswd().equals(passwd)
                ? usersLoginAndUser.get(login).getUserKey() : null;
    }

    public User getUserByUserKey(String userKey) {
        return usersKeyAndUser.get(userKey);
    }

    public void setUpdateUser(User updateUser) throws ResponceExceptionImpl {
        try {
            userService.setUpdateUser(updateUser);
        } catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.EXCEPTION, e.getMessage());
        }
    }

    public boolean isUserLogin(String login){
        return usersLoginAndUser.containsKey(login);
    }

    /**
     * Добавление нового пользователя
     * @param newUser
     */
    public void registNewUser(User newUser) throws ResponceExceptionImpl {

        if(!usersLoginAndUser.containsKey(newUser.getLogin())){
            try {
                User user = userService.setUser(newUser);
                users.put(user.getId(), user);
                usersLoginAndUser.put(user.getLogin(),user);
                usersKeyAndUser.put(user.getUserKey(),user);

            }catch (ResponceExceptionImpl e) {
                LOGGER.error(e);
                throw new ResponceExceptionImpl(TypeException.EXCEPTION, e.getMessage());
            }
        }else {
            LOGGER.error("User already this");
        }

    }

    protected List<String> getAdminsEmail(){

        List<String> emailAdminsList = new LinkedList<>();

        users.values().forEach(user -> {
            if(RolesType.ADMIN.isRole(user.getRole())){
                emailAdminsList.add(user.getEmail());
            }
        });

        return emailAdminsList;
    }

}
