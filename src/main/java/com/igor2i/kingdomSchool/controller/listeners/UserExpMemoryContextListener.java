package com.igor2i.kingdomSchool.controller.listeners;

import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.db.modelsBO.UserExperience;
import com.igor2i.kingdomSchool.services.UserExperienceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 27.02.17.
 */
@WebListener
@Service
public class UserExpMemoryContextListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(UserMemoryServletContextListener.class);

    @Autowired
    private UserExperienceService userExperienceService;

    //TODO сделать синхронизацию изменений в пуле
    private static ConcurrentHashMap<Long, UserExperience> usersIdAndExp;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOGGER.debug("init UserExpDBContextListener");

        WebApplicationContextUtils
                .getRequiredWebApplicationContext(sce.getServletContext())
                .getAutowireCapableBeanFactory()
                .autowireBean(this);

        try {

            usersIdAndExp = userExperienceService.getAll();

        }catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
            contextDestroyed(sce);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOGGER.debug("destroy UserExpDBContextListener");
    }

    public Integer getUserExp(Long userId){
        return getUserExperience(userId).getExperience();
    }

    public Integer addUserExp(Long userId, int exp){
        UserExperience userExperience = getUserExperience(userId);
        userExperience.setExperience( userExperience.getExperience() + exp);
        //TODO пока так
        try {
            userExperienceService.setUpdateUserExp(userExperience);
            LOGGER.debug("User id " + userExperience.getIdUser() + " got " + exp + " experience");
        } catch (ResponceExceptionImpl e) {
            LOGGER.error("Не удалось обновить данные опыта в БД для пользовтеля c id " + userExperience.getIdUser()
                    + ": " + e.getMessage());
        }

        return userExperience.getExperience();
    }

    private UserExperience getUserExperience(Long userId){
        if(usersIdAndExp.containsKey(userId)){
            return usersIdAndExp.get(userId);
        }else {
            try {
                UserExperience userExperience = userExperienceService.getUserExp(userId);
                if(userExperience != null){
                    usersIdAndExp.put(userExperience.getIdUser(),userExperience);
                    return userExperience;
                }else {
                    throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, "User exp not found");
                }

            } catch (ResponceExceptionImpl responceException) {
                LOGGER.warn(responceException);
                return null;
            }
        }
    }
}
