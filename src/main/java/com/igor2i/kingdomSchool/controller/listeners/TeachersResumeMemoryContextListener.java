package com.igor2i.kingdomSchool.controller.listeners;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.modelsBO.TeacherResume;
import com.igor2i.kingdomSchool.services.TeachersResumeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 02.03.17.
 */
@WebListener
@Service
public class TeachersResumeMemoryContextListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(UserMemoryServletContextListener.class);

    @Autowired
    private TeachersResumeService teachersResumeService;
    @Autowired
    private UserExpMemoryContextListener userExpMemoryContextListener;

    private static ConcurrentHashMap<Long, TeacherResume> resumeConcurrentHashMap;


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOGGER.debug("init TeachersResumeMemoryContextListener");

        WebApplicationContextUtils
                .getRequiredWebApplicationContext(sce.getServletContext())
                .getAutowireCapableBeanFactory()
                .autowireBean(this);

        try {
            resumeConcurrentHashMap = teachersResumeService.getAll();
        } catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
            contextDestroyed(sce);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOGGER.debug("destroy TeachersResumeMemoryContextListener");

    }

    public void setUpdateResume(TeacherResume teacherResume) throws ResponceExceptionImpl {
        resumeConcurrentHashMap.replace(teacherResume.getIduser(), teacherResume);
        try {
            teachersResumeService.setUpdateTeacherResume(teacherResume);
        } catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.EXCEPTION, e.getMessage());
        }
    }

    public TeacherResume getTeacherResumeByUserId(long idUser) {
        return getTeacherResume(idUser);
    }

    public ArrayList<Map.Entry<Integer, TeacherResume>> getSortByRatingLesson() {

        ArrayList<Map.Entry<Integer, TeacherResume>> sortedLessonsByMaxRating = new ArrayList<>(resumeConcurrentHashMap.size());

        resumeConcurrentHashMap.values().forEach(r -> {

            sortedLessonsByMaxRating.add(new Map.Entry<Integer, TeacherResume>() {
                @Override
                public Integer getKey() {
                    return userExpMemoryContextListener.getUserExp(r.getIduser());
                }

                @Override
                public TeacherResume getValue() {
                    return r;
                }

                @Override
                public TeacherResume setValue(TeacherResume value) {
                    return value;
                }
            });
        });

        sortedLessonsByMaxRating.sort(Collections.reverseOrder(Comparator.comparingInt(Map.Entry::getKey)));

        return sortedLessonsByMaxRating;
    }

    private TeacherResume getTeacherResume(Long userId){
        if(resumeConcurrentHashMap.containsKey(userId)){
            return resumeConcurrentHashMap.get(userId);
        }else {
            try {
                TeacherResume teacherResume = teachersResumeService.getTeacherResumeById(userId);
                if(teacherResume != null){
                    resumeConcurrentHashMap.put(teacherResume.getIduser(),teacherResume);
                    return teacherResume;
                }else {
                    throw new ResponceExceptionImpl(TypeException.SQL_EXCEPTION, "Teacher resume not found");
                }

            } catch (ResponceExceptionImpl responceException) {
                LOGGER.warn(responceException);
                return null;
            }
        }
    }


}
