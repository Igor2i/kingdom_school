package com.igor2i.kingdomSchool.controller.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebServlet;
import java.io.File;

/**
 * Created by igor2i on 25.02.17.
 */
@WebServlet
public class FileContextListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(FileContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        String rootPath = System.getProperty("catalina.home");
        ServletContext ctx = sce.getServletContext();
        String relativePath = ctx.getInitParameter("temp.dir");

        File file = new File(rootPath + File.separator + relativePath);
        if(!file.exists()) {
            file.mkdirs();
            LOGGER.info("Create directory " + relativePath);
        }

        ctx.setAttribute("FILES_DIR_FILE", file);
        ctx.setAttribute("FILES_DIR", rootPath + File.separator + relativePath);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
