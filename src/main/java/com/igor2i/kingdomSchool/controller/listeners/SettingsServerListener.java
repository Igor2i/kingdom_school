package com.igor2i.kingdomSchool.controller.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by igor2i on 27.02.17.
 */
@Deprecated
public class SettingsServerListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(SettingsServerListener.class);

    private static boolean notifyAdminAboutLogin = true;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOGGER.debug("init SettingsServerListener");

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    public static boolean isNotifyAdminAboutLogin() {
        return notifyAdminAboutLogin;
    }

    public static void setNotifyAdminAboutLogin(boolean notifyAdminAboutLogin) {
        SettingsServerListener.notifyAdminAboutLogin = notifyAdminAboutLogin;
    }
}
