package com.igor2i.kingdomSchool.controller.enums;

/**
 * Created by igor2i on 25.02.17.
 */
public enum RolesType {

    ADMIN {
        @Override
        public boolean isRole(Integer role) {
            return role == 1;
        }
    },
    MANAGER {
        @Override
        public boolean isRole(Integer role) {
            return role == 2;
        }
    },
    TEACHER {
        @Override
        public boolean isRole(Integer role) {
            return role == 3;
        }
    },
    UP_STUDENT {
        @Override
        public boolean isRole(Integer role) {
            return role == 4;
        }
    },
    STUDENT {
        @Override
        public boolean isRole(Integer role) {
            return role == 5;
        }
    };

    public abstract boolean isRole(Integer role);

}
