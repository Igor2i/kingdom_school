package com.igor2i.kingdomSchool.controller.enums;

/**
 * Created by igor2i on 27.02.17.
 */
public enum LessonType {

    ONLY_TEXT, VIDEO, INTERACTIVE, OTHER;


    public static LessonType whatTypeThisLesson(int iType){

        switch (iType) {
            case 1: return ONLY_TEXT;
            case 2: return VIDEO;
            case 3: return INTERACTIVE;
            default: return OTHER;
        }

    }
}
