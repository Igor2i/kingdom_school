package com.igor2i.kingdomSchool.controller.enums;

/**
 * Created by igor2i on 25.02.17.
 */
public enum TypeEvent {
    NOT_SUPPORT_FIELD {
        @Override
        public String getMsg() {
            return null;
        }
    }, ERROR_EMAIL {
        @Override
        public String getMsg() {
            return "Введите корректный Email";
        }

    }, ERROR_LOGIN {
        @Override
        public String getMsg() {
            return "Введите корректный логин";
        }

    }, ERROR_PASSWORD {
        @Override
        public String getMsg() {
            return "Введите корректный пароль";
        }

    }, ERROR {
        @Override
        public String getMsg() {
            return null;
        }

    }, ERROR_LOGIN_CONTAIN {
        @Override
        public String getMsg() {
            return "Данный логин уже занят";
        }

    }, ERROR_DATE_TYPE {
        @Override
        public String getMsg() {
            return "Введите корректную дату рождения";
        }
    },
    EVENT_HELLO_NEW_USER {
        @Override
        public String getMsg() {
            return "Добро пожаловать на вашу почту было высланно подтверждение регистрации";
        }

    },
    FOLDER_DENIED {
        @Override
        public String getMsg() {
            return "403 доступ запрещён";
        }
    },
    ERROR_ACCESS_LOGIN_OR_PASSWORD {
        @Override
        public String getMsg() {
            return "Ошибка входа, введён некоректный логин или пароль";
        }
    },
    ERROR_CREATE_USER {
        @Override
        public String getMsg() {
            return "Во время создания Вашей учётной записи произошла ошибка, мы приносим извенения попробуйте позднее.";
        }
    },
    ERROR_UPDATE_USER {
        @Override
        public String getMsg() {
            return "Во время редактирования Вашей учётной записи произошла ошибка, мы приносим извенения попробуйте позднее.";
        }
    },
    UPDATE_PROFILE_SUCCESS {
        @Override
        public String getMsg() {
            return "Профиль успешно обновлён";
        }
    };


    public abstract String getMsg();

}
