package com.igor2i.kingdomSchool.controller;

import com.igor2i.kingdomSchool.controller.listeners.CatcherOnlineUserSesAttrListener;
import com.igor2i.kingdomSchool.services.AuthService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by igor2i on 27.02.17.
 */
@WebServlet(urlPatterns = "/admin/setting",
        initParams = {@WebInitParam(name="notifyAdminAboutLogin", value="1")})
@Component
public class ASettingServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(ASettingServlet.class);

    private boolean isNotifyAdminAboutLogin = true;


    @Autowired
    private AuthService authService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        LOGGER.debug("init ASettingServlet");
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
        isNotifyAdminAboutLogin = "1".equals(config.getInitParameter("notifyAdminAboutLogin"));
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("index getASetting " + req.getRequestURI());
        req.setCharacterEncoding("UTF-8");
        authService.addAttribAuthUser(req);
        setSettingServer(req);
        req.getRequestDispatcher("/admin/adminSettingPage.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("index postMethod " + req.getRequestURI());
        setSettingServer(req);
        boolean notifyAdminsState = "1".equals(req.getParameter("notifyAdmins"));
        setNotifyAdminAboutLogin(notifyAdminsState);
        req.getRequestDispatcher("/admin/adminSettingPage.jsp").forward(req, resp);
    }

    private void setSettingServer(HttpServletRequest req){
        req.setAttribute("notifyAdmins", isNotifyAdminAboutLogin );
    }

    private void setNotifyAdminAboutLogin(boolean notifyParam){
        LOGGER.debug("set notify about admins login is " + String.valueOf(notifyParam));
        isNotifyAdminAboutLogin = notifyParam;
        setForAllNotifyAdminAboutLogin();
    }

    public void setForAllNotifyAdminAboutLogin(){
        if(isNotifyAdminAboutLogin){
            CatcherOnlineUserSesAttrListener.isEventLoginForAdmins = true;
        }else {
            CatcherOnlineUserSesAttrListener.isEventLoginForAdmins = false;
        }
    }
}
