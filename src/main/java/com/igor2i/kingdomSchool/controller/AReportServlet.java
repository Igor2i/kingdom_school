package com.igor2i.kingdomSchool.controller;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

/**
 * Created by igor2i on 22.02.17.
 */
@WebServlet("/admin/report")
public class AReportServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(AReportServlet.class);

    private ServletFileUpload uploader = null;

    @Override
    public void init() throws ServletException {
        DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        File filesDir = (File) getServletContext().getAttribute("FILES_DIR_FILE");
        fileFactory.setRepository(filesDir);
        this.uploader = new ServletFileUpload(fileFactory);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("a_report " + req.getRemoteUser());


//        String fileName = req.getParameter("fileName");
//        if (fileName == null || fileName.equals("")) {
//            throw new ServletException("File Name can't be null or empty");
//        }
//        File file = new File(req.getServletContext().getAttribute("FILES_DIR") + File.separator + fileName);
//        if (!file.exists()) {
//            throw new ServletException("File doesn't exists on server.");
//        }
//        System.out.println("File location on server::" + file.getAbsolutePath());
//        ServletContext ctx = getServletContext();
//        InputStream fis = new FileInputStream(file);
//        String mimeType = ctx.getMimeType(file.getAbsolutePath());
//        resp.setContentType(mimeType != null ? mimeType : "application/octet-stream");
//        resp.setContentLength((int) file.length());
//        resp.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//
//        ServletOutputStream os = resp.getOutputStream();
//        byte[] bufferData = new byte[1024];
//        int read = 0;
//        while ((read = fis.read(bufferData)) != -1) {
//            os.write(bufferData, 0, read);
//        }
//        os.flush();
//        os.close();
//        fis.close();
//        System.out.println("File downloaded at client successfully");
//

        req.getRequestDispatcher("/admin/adminReportPage.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if(!ServletFileUpload.isMultipartContent(req)){
            throw new ServletException("Content type is not multipart/form-data");
        }

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.write("<html><head></head><body>");
        try {
            List<FileItem> fileItemsList = uploader.parseRequest(req);
            Iterator<FileItem> fileItemsIterator = fileItemsList.iterator();
            while(fileItemsIterator.hasNext()){
                FileItem fileItem = fileItemsIterator.next();
                System.out.println("FieldName="+fileItem.getFieldName());
                System.out.println("FileName="+fileItem.getName());
                System.out.println("ContentType="+fileItem.getContentType());
                System.out.println("Size in bytes="+fileItem.getSize());

                File file = new File(req.getServletContext().getAttribute("FILES_DIR")+File.separator+fileItem.getName());
                System.out.println("Absolute Path at server="+file.getAbsolutePath());
                fileItem.write(file);
                out.write("File "+fileItem.getName()+ " uploaded successfully.");
                out.write("<br>");
                out.write("<a href=\"UploadDownloadFileServlet?fileName="+fileItem.getName()+"\">Download "+fileItem.getName()+"</a>");
            }
        } catch (FileUploadException e) {
            out.write("Exception in uploading file.");
        } catch (Exception e) {
            out.write("Exception in uploading file.");
        }
        out.write("</body></html>");

    }
}
