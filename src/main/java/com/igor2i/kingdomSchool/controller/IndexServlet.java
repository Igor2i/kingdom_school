package com.igor2i.kingdomSchool.controller;

import com.igor2i.kingdomSchool.controller.listeners.LessonsMemoryContextListener;
import com.igor2i.kingdomSchool.db.modelsBO.Lessons;
import com.igor2i.kingdomSchool.services.AuthService;
import com.igor2i.kingdomSchool.services.HTMLGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by igor2i on 22.02.17.
 */
@WebServlet("")
@Component
public class IndexServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(IndexServlet.class);

    @Autowired
    private AuthService authService;
    @Autowired
    private LessonsMemoryContextListener lessonsMemoryContextListener;
    @Autowired
    private HTMLGenerator htmlGenerator;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LOGGER.debug("index " + req.getRequestURI());

        authService.addAttribAuthUser(req);
        htmlGenerator.setHeadPage(req, "KinGDom School");
        htmlGenerator.setAboutModulePage(req);
        htmlGenerator.setFooterScriptsPage(req);
        htmlGenerator.setHeaderFirstMenuPage(req);

        setShortLessonsIntoHtmlPage(req);

        req.getRequestDispatcher("index.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("index postMethod " + req.getRequestURI());

        authService.addAttribAuthUser(req);
        htmlGenerator.setHeadPage(req, "KinGDom School");
        htmlGenerator.setAboutModulePage(req);
        htmlGenerator.setFooterScriptsPage(req);
        htmlGenerator.setHeaderFirstMenuPage(req);

        setShortLessonsIntoHtmlPage(req);

        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

    private void setShortLessonsIntoHtmlPage(HttpServletRequest req) {

        StringBuilder htmlTemplate = new StringBuilder();

        ArrayList<Map.Entry<Integer, Lessons>> lessons = lessonsMemoryContextListener.getSortByRatingLesson();

        int colLessonOnPage = lessons.size() >= 12 ? 12 : lessons.size();

        for (int i = 0; i < colLessonOnPage; i++) {

            htmlTemplate.append("<div class=\"col-md-4 col-sm-6\">");
            htmlTemplate.append("<div class=\"panel panel-default\">"); // для каждой лекции
            //header lesson
            htmlTemplate.append("<div class=\"panel-heading\"><a href=\"");
            if(req.getSession().getAttribute("userKey") != null) {
                htmlTemplate.append("/lesson?id=").append(lessons.get(i).getValue().getId()).append('"');
            }else {
                htmlTemplate.append("#loginModal\" data-toggle=\"modal\"");
            }
            htmlTemplate.append(" class=\"pull-right\">Открыть</a><h4>")
                    .append(lessons.get(i).getValue().getHeader())
                    .append("</h4></div>");
            htmlTemplate.append("<div class=\"panel-body\">")
                    .append(lessons.get(i).getValue().getShortDescription())
                    .append("</div>");
            htmlTemplate.append("</div>"); // <div class="panel panel-default">
            htmlTemplate.append("</div>"); //<div class="col-md-4 col-sm-6">

        }

        req.setAttribute("htmlLessonsBlock", htmlTemplate);

    }



}
