package com.igor2i.kingdomSchool.controller;

import com.igor2i.kingdomSchool.controller.enums.TypeEvent;
import com.igor2i.kingdomSchool.controller.listeners.UserMemoryServletContextListener;
import com.igor2i.kingdomSchool.services.EventService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by igor2i on 22.02.17.
 */
@WebServlet("/auth")
@Component
public class AuthServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(AuthServlet.class);

    @Autowired
    private UserMemoryServletContextListener userMemoryServletContextListener;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("logout " + req.getRequestURI());

        HttpSession session = req.getSession();
        session.removeAttribute("userKey");

        resp.sendRedirect("/");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("login");
        String passwd = req.getParameter("passwd");

        String userKey = userMemoryServletContextListener.getUserKey(login, cryptStringToSHA256(passwd));
        if(userKey == null){
            LOGGER.debug("Failed access with login '" + login + "' pass '" + passwd+"'");

            EventService.addAttribEvent(req, TypeEvent.ERROR_ACCESS_LOGIN_OR_PASSWORD);
            req.getRequestDispatcher("/").forward(req, resp);

        }else {

            LOGGER.debug("Login user " + login + " token " + userKey);

            req.getSession().setAttribute("userKey", userKey);

            resp.sendRedirect("/");
        }
    }

    private String cryptStringToSHA256(String text) {

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
            String servernayaSol = "My dobavim nemnogo soli dlya etogo POLZOVATELYZ";
            md.update((text + servernayaSol).getBytes("UTF-8"));
            byte[] cryptBytes = md.digest();

            return String.format("%064x", new java.math.BigInteger(1, cryptBytes));

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            LOGGER.error("Fail crypt " + e.getMessage());
            return text;
        }
    }
}
