package com.igor2i.kingdomSchool.importer;

import com.igor2i.kingdomSchool.commons.errors.impls.ResponceExceptionImpl;
import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.db.LessonsInDB;
import com.igor2i.kingdomSchool.db.UsersInDB;
import com.igor2i.kingdomSchool.parser.Parser;
import com.igor2i.kingdomSchool.parser.containers.LessonsContainer;
import com.igor2i.kingdomSchool.parser.containers.UsersContainer;
import com.igor2i.kingdomSchool.parser.impl.JaxbParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

/**
 * Created by igor2i on 20.02.17.
 */
public class ImportFromXMLToDB {

    private static final Logger LOGGER = LogManager.getLogger(ImportFromXMLToDB.class);

    public void importUsers(File file) throws ResponceExceptionImpl {

        try {

            Parser parser = new JaxbParser();
            UsersContainer usC = (UsersContainer) parser.getObject(file, UsersContainer.class);

            usC.getUserList().forEach(user -> LOGGER.debug(user.toString()));

            UsersInDB usersInDB = new UsersInDB();
            usersInDB.importToDB(usC.getUserList());

        } catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.EXCEPTION, e.getMessage());
        }
    }

    public void importLessons(File file) throws ResponceExceptionImpl {

        try {

            Parser parser = new JaxbParser();
            LessonsContainer lessonsC = (LessonsContainer) parser.getObject(file, LessonsContainer.class);

            lessonsC.getLessonsList().forEach(user -> LOGGER.debug(user.toString()));

            LessonsInDB lessonsInDB = new LessonsInDB();
            lessonsInDB.importToDB(lessonsC.getLessonsList());

        } catch (ResponceExceptionImpl e) {
            LOGGER.error(e);
            throw new ResponceExceptionImpl(TypeException.EXCEPTION, e.getMessage());
        }


    }

}
