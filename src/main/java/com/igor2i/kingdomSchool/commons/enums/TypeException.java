package com.igor2i.kingdomSchool.commons.enums;

/**
 * Created by igor2i on 01.03.17.
 */
public enum  TypeException {
    SQL_EXCEPTION {
        @Override
        public String getMessage() {
            return "SQL exception";
        }
    }, EXCEPTION {
        @Override
        public String getMessage() {
            return "Other exception";
        }
    },
    FILE_IO_EXCEPTION {
        @Override
        public String getMessage() {
            return "File.IO exception";
        }
    },
    PARSE_EXCEPTION {
        @Override
        public String getMessage() {
            return "Parse exception";
        }
    },
    USER_ANSWER {
        @Override
        public String getMessage() {
            return "У нас что-то пошло не так, но мы уже работаем над этим.";
        }
    },
    SESSION_ERROR {
        @Override
        public String getMessage() {
            return "Ошибка сессии";
        }
    };

    public abstract String getMessage();
}
