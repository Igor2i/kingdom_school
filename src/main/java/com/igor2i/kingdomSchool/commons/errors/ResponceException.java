package com.igor2i.kingdomSchool.commons.errors;

import com.igor2i.kingdomSchool.commons.enums.TypeException;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by igor2i on 05.03.17.
 */
public interface ResponceException {

    String getMessage();

    void responceClientErrorPage(HttpServletResponse resp, TypeException typeException);

    TypeException getTypeException();

    void setTypeException(TypeException typeException);

    String getThrowMessage();

    void setThrowMessage(String throwMessage);

}
