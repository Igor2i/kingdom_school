package com.igor2i.kingdomSchool.commons.errors.impls;

import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.commons.errors.ResponceException;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by igor2i on 25.02.17.
 */
public class ResponceExceptionImpl extends Exception implements ResponceException{

    private TypeException typeException = TypeException.EXCEPTION;
    private String throwMessage = "";

    public ResponceExceptionImpl() {
    }

    public ResponceExceptionImpl(TypeException typeException) {
        this.typeException = typeException;
    }

    public ResponceExceptionImpl(TypeException typeException, String throwMessage) {
        this.typeException = typeException;
        this.throwMessage = throwMessage;
    }

    @Override
    public String getMessage() {
        return typeException.getMessage() + ":  " + throwMessage;
    }

    public void responceClientErrorPage(HttpServletResponse resp, TypeException typeException){

    }

    public TypeException getTypeException() {
        return typeException;
    }

    public void setTypeException(TypeException typeException) {
        this.typeException = typeException;
    }

    public String getThrowMessage() {
        return throwMessage;
    }

    public void setThrowMessage(String throwMessage) {
        this.throwMessage = throwMessage;
    }
}
