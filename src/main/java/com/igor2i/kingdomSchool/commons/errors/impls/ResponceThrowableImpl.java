package com.igor2i.kingdomSchool.commons.errors.impls;

import com.igor2i.kingdomSchool.commons.enums.TypeException;
import com.igor2i.kingdomSchool.commons.errors.ResponceException;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by igor2i on 05.03.17.
 */
public class ResponceThrowableImpl extends Throwable implements ResponceException {


    private TypeException typeException = TypeException.EXCEPTION;
    private String throwMessage = "";

    public ResponceThrowableImpl() {
    }

    public ResponceThrowableImpl(TypeException typeException) {
        this.typeException = typeException;
    }

    public ResponceThrowableImpl(TypeException typeException, String throwMessage) {
        this.typeException = typeException;
        this.throwMessage = throwMessage;
    }

    @Override
    public void responceClientErrorPage(HttpServletResponse resp, TypeException typeException) {

    }

    @Override
    public TypeException getTypeException() {
        return null;
    }

    @Override
    public void setTypeException(TypeException typeException) {

    }

    @Override
    public String getThrowMessage() {
        return null;
    }

    @Override
    public void setThrowMessage(String throwMessage) {

    }
}
