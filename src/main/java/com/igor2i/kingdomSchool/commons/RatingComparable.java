package com.igor2i.kingdomSchool.commons;

/**
 * Created by igor2i on 26.02.17.
 */
@Deprecated
public class RatingComparable implements Comparable{

    private Integer rating;
    private Object object;

    public RatingComparable(Integer rating, Object object) {
        this.rating = rating;
        this.object = object;
    }

    public Integer getRating() {
        return rating;
    }

    public Object getObject() {
        return object;
    }


    @Override
    public int compareTo(Object obj) {
        RatingComparable entry = (RatingComparable) obj;

        int result = entry.rating - rating;
        if(result != 0) {
            return (int) result / Math.abs( result );
        }
        return 0;
    }

}
