package com.igor2i.kingdomSchool.commons.aop.logger;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * Created by igor2i on 04.03.17.
 */
@Component
@Aspect
public class LoggerAOP {

    @Pointcut("execution(* *(..))")
    private void allMethods(){}



}
