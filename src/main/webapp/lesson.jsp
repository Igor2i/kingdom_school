<%@ page import="com.igor2i.kingdomSchool.controller.enums.RolesType" %><%--
  Created by IntelliJ IDEA.
  User: igor2i
  Date: 26.02.17
  Time: 23:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>KingDom School</title>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="/css/styles.css" rel="stylesheet">
</head>


<body>

<nav class="navbar navbar-fixed-top header">
    <div class="col-md-12">
        <div class="navbar-header">

            <a href="/"><img src="/img/Logo.jpg" height="45px"></a>

        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" id="btnToggle" class="social-bar"><i class="glyphicon glyphicon-th-large"></i></a></li>
                <li><a href="#" class="social-bar"><img src="/img/socail_f.jpg" height="35px"></a></li>
                <li><a href="#" class="social-bar"><img src="/img/socail_t.jpg" height="35px"></a></li>
                <li><a href="#" class="social-bar"><img src="/img/socail_i.jpg" height="35px"></a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="navbar navbar-default" id="subnav">
    <div class="col-md-12">
        <div class="navbar-header">

            <a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus dropdown-toggle"
               data-toggle="dropdown">&#9776; Home
                <%--<small><i class="glyphicon glyphicon-chevron-down"></i></small>--%>
            </a>
            <ul class="nav dropdown-menu">
                <li><a href="#"> Profile</a></li>
                <li><a href="#"> Dashboard</a></li>
                <li><a href="#"> Pages</a></li>
                <%if(  request.getAttribute("userRole") != null){
                    if(RolesType.ADMIN.isRole((Integer) request.getAttribute("userRole"))){ %>
                <li class="nav-divider"></li>
                <li><a href="/admin"> Админка</a></li>
                <li><a href="/admin/setting"> Настройки сервера</a></li>
                <li><a href="/admin/report"> Отчёты</a></li>
                <%}else if(RolesType.MANAGER.isRole((Integer) request.getAttribute("userRole"))){%>
                <li class="nav-divider"></li>
                <li><a href="/admin"> Админка</a></li>
                <li><a href="/admin/report"> Отчёты</a></li>
                <%}else if(RolesType.TEACHER.isRole((Integer) request.getAttribute("userRole"))){%>
                <li class="nav-divider"></li>
                <li><a href="/admin"> Админка</a></li>
                <li><a href="/admin/report"> Отчёты</a></li>
                <%}}%>
            </ul>


            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

        <a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus navbar-right dropdown-toggle"
           data-toggle="dropdown">
            <%="Hello " + request.getAttribute("userFIO")%> &#9660;
        </a>
        <ul class="nav dropdown-menu dropdown-menu-right">
            <li><a href="#">&#9998;Profile</a></li>
            <li><a href="#">Settings</a></li>
            <li class="nav-divider"></li>
            <li><a href="/auth">Logout</a></li>
        </ul>

    </div>
</div>

<!--main-->
<div class="container" id="main">
    <% if(request.getAttribute("event") != null){%>
    <%=request.getAttribute("event")%>
    <% } %>
    <div class="row">

        <%if(request.getAttribute("headerLesson") != null && request.getAttribute("bodyLesson") != null){%>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2><%=request.getAttribute("headerLesson")%></h2>
            </div>
            <div class="panel-body">
                <%=request.getAttribute("bodyLesson")%>
            </div>

            <%=request.getAttribute("readyBlock") != null ? request.getAttribute("readyBlock") : "" %>
        </div>
        <%}%>



    </div><!--/row-->


    <hr>

    <div class="row">
        <div class="col-md-12"><h2>Playground</h2></div>
        <div class="col-md-12">
            <!-- ALERT -->
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="#" class="pull-right">View all</a> <h4>Buttons &amp; Labels</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-4"><a class="btn btn-default center-block" href="#">Button</a></div>
                        <div class="col-xs-4"><a class="btn btn-primary center-block" href="#">Primary</a></div>
                        <div class="col-xs-4"><a class="btn btn-danger center-block" href="#">Danger</a></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4"><a class="btn btn-warning center-block" href="#">Warning</a></div>
                        <div class="col-xs-4"><a class="btn btn-info center-block" href="#">Info</a></div>
                        <div class="col-xs-4"><a class="btn btn-success center-block" href="#">Success</a></div>
                    </div>
                    <hr>
                    <div class="btn-group btn-group-sm">
                        <button class="btn btn-default">1</button>
                        <button class="btn btn-default">2</button>
                        <button class="btn btn-default">3</button>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <span class="label label-default">Label</span>
                            <span class="label label-success">Success</span>

                        </div>
                        <div class="col-md-4">
                            <span class="label label-warning">Warning</span>
                            <span class="label label-info">Info</span>
                        </div>
                        <div class="col-md-4">
                            <span class="label label-danger">Danger</span>
                            <span class="label label-primary">Primary</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="#" class="pull-right">View all</a> <h4>Progress Bars</h4></div>
                <div class="panel-body">

                    <div class="progress">
                        <div class="progress-bar progress-bar-info" style="width: 20%"></div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" style="width: 40%"></div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning" style="width: 80%"></div>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger" style="width: 50%"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="#" class="pull-right">View all</a> <h4>Tabs</h4></div>
                <div class="panel-body">

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#A" data-toggle="tab">Section 1</a></li>
                        <li><a href="#B" data-toggle="tab">Section 2</a></li>
                        <li><a href="#C" data-toggle="tab">Section 3</a></li>
                    </ul>
                    <div class="tabbable">
                        <div class="tab-content">
                            <div class="tab-pane active" id="A">
                                <div class="well well-sm">I'm in Section A.</div>
                            </div>
                            <div class="tab-pane" id="B">
                                <div class="well well-sm">Howdy, I'm in Section B.</div>
                            </div>
                            <div class="tab-pane" id="C">
                                <div class="well well-sm">I've decided that I like wells.</div>
                            </div>
                        </div>
                    </div> <!-- /tabbable -->

                    <div class="col-sm-12 text-center">
                        <ul class="pagination center-block" style="display:inline-block;">
                            <li><a href="#">«</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">»</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div><!--playground-->

        <br>

        <div class="clearfix"></div>

        <hr>
        <div class="col-md-12 text-center"><p>Сайт создан Крикуновым Игорем<br>
            <a href="https://bitbucket.org/Igor2i" target="_ext">Мой Git</a></p></div>
        <hr>

    </div>
</div><!--/main-->



<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/scripts.js"></script>
</body>
</html>

