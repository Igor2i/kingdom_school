<%@ page import="com.igor2i.kingdomSchool.controller.enums.RolesType" %><%--
  Created by IntelliJ IDEA.
  User: igor2i
  Date: 02.03.17
  Time: 17:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<%=request.getAttribute("head")%>

<body>

<%=request.getAttribute("headerFirstMenu")%>

<div class="navbar navbar-default" id="subnav">
    <div class="col-md-12">
        <div class="navbar-header">

            <a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus dropdown-toggle"
               data-toggle="dropdown">&#9776; Home
                <%--<small><i class="glyphicon glyphicon-chevron-down"></i></small>--%>
            </a>
            <ul class="nav dropdown-menu">
                <li><a href="#"> Profile</a></li>
                <li><a href="#"> Dashboard</a></li>
                <li><a href="#"> Pages</a></li>
                <li class="nav-divider"></li>
                <li><a href="#"> Settings</a></li>
                <li><a href="#"> More..</a></li>
            </ul>


            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <%
            if (request.getSession().getAttribute("userKey") == null) {
        %>
        <div class="collapse navbar-collapse" id="navbar-collapse2">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#loginModal" role="button" data-toggle="modal">Login</a></li>
                <li><a href="/registration" role="button" data-toggle="modal">Registration</a></li>
                <li><a href="#aboutModal" role="button" data-toggle="modal">About</a></li>
            </ul>
        </div>
        <% } else { %>
        <%--<a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus dropdown-toggle" data-toggle="dropdown">--%>
        <a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus navbar-right dropdown-toggle"
           data-toggle="dropdown">
            <%="Hello " + request.getAttribute("userFIO")%> &#9660;
        </a>
        <ul class="nav dropdown-menu dropdown-menu-right">
            <li><a href="/profile-edit">&#9998;Profile</a></li>
            <li><a href="#">Settings</a></li>
            <li class="nav-divider"></li>
            <li><a href="/auth">Logout</a></li>
        </ul>
        <% }%>

    </div>
</div>

<!--main-->
<div class="container" id="main">

    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading"><h3>Регистрация</h3></div>
            <!-- ALERT -->
            <% if(request.getAttribute("event") != null){%>
            <%=request.getAttribute("event")%>
            <% } %>
            <!-- ----- -->
            <div class="panel-body">
                <form action="/profile-edit" method="post">
                    <table class="table">
                        <tr>
                            <td><label for="email">Email:</label></td>
                            <td><input type="email" name="email" id="email" value="<%=request.getAttribute("email")%>" placeholder="example@email.ru"></td>
                        </tr>
                        <tr>
                            <td><label for="lastname">Фамилия:</label></td>
                            <td><input type="text" name="lastname" id="lastname" value="<%=request.getAttribute("lastname")%>" placeholder="Фамилия"></td>
                        </tr>
                        <tr>
                            <td><label for="firstname">Имя:</label></td>
                            <td><input type="text" name="firstname" id="firstname" value="<%=request.getAttribute("firstname")%>" placeholder="Имя"></td>
                        </tr>
                        <tr>
                            <td><label for="male">Пол:</label></td>
                            <td><label>Мужчина
                                <input type="radio" name="male" id="male" value="1" <%=(boolean)request.getAttribute("male") ? "checked" : ""%>></label>
                                <label>Девушка
                                    <input type="radio" name="male" id="male" value="0" <%=!(boolean)request.getAttribute("male") ? "checked" : ""%>></label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="vklink">VK link:</label></td>
                            <td><input type="text" name="vklink" id="vklink" value="<%=request.getAttribute("vklink")%>" placeholder="Input"></td>
                        </tr>
                        <%if( request.getAttribute("userRole") != null &&
                                (RolesType.TEACHER.isRole((Integer) request.getAttribute("userRole")) ||
                                        RolesType.UP_STUDENT.isRole((Integer) request.getAttribute("userRole")) )){%>
                            <tr>
                                <td><label for="resumeHead">Укажите заголовок резюме:</label></td>
                                <td><input type="text" name="resumeHead" id="resumeHead" value="<%=request.getAttribute("resumeHead")%>" placeholder="Input"></td>
                            </tr>
                            <tr>
                                <td><label for="resumeBody">Укажите заголовок резюме:</label></td>
                                <td><textarea name="resumeBody" id="resumeBody"  rows="10" cols="70"><%=request.getAttribute("resumeBody")%></textarea></td>
                            </tr>
                        <%}%>
                    </table>


                    <br>

                    <input type="submit" class="btn btn-success center-block" value="Обновить профиль">
                </form>

            </div>
        </div>

        <br>

        <div class="clearfix"></div>

        <hr>
        <div class="col-md-12 text-center"><p>Сайт создан Крикуновым Игорем<br>
            <a href="https://bitbucket.org/Igor2i" target="_ext">Мой Git</a></p></div>
        <hr>

    </div>
</div><!--/main-->


<!-- script references -->
<%=request.getAttribute("footerScripts")%>

</body>
</html>