<%--
  Created by IntelliJ IDEA.
  User: igor2i
  Date: 27.02.17
  Time: 23:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>KingDom School</title>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="/css/styles.css" rel="stylesheet">
</head>


<body>

<nav class="navbar navbar-fixed-top header">
    <div class="col-md-12">
        <div class="navbar-header">

            <a href="/"><img src="/img/Logo.jpg" height="45px"></a>

        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" id="btnToggle" class="social-bar"><i class="glyphicon glyphicon-th-large"></i></a></li>
                <li><a href="#" class="social-bar"><img src="/img/socail_f.jpg" height="35px"></a></li>
                <li><a href="#" class="social-bar"><img src="/img/socail_t.jpg" height="35px"></a></li>
                <li><a href="#" class="social-bar"><img src="/img/socail_i.jpg" height="35px"></a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="navbar navbar-default" id="subnav">
    <div class="col-md-12">
        <div class="navbar-header">

            <a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus dropdown-toggle"
               data-toggle="dropdown">&#9776; Home
                <%--<small><i class="glyphicon glyphicon-chevron-down"></i></small>--%>
            </a>
            <ul class="nav dropdown-menu">
                <li><a href="#"> Profile</a></li>
                <li><a href="#"> Dashboard</a></li>
                <li><a href="#"> Pages</a></li>
                <li class="nav-divider"></li>
                <li><a href="/admin"> Админка</a></li>
                <li><a href="/admin/setting"> Настройки сервера</a></li>
                <li><a href="/admin/report"> Отчёты</a></li>
            </ul>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus navbar-right dropdown-toggle"
           data-toggle="dropdown">
            <%="Hello " + request.getAttribute("userFIO")%> &#9660;
        </a>
        <ul class="nav dropdown-menu dropdown-menu-right">
            <li><a href="#">&#9998;Profile</a></li>
            <li><a href="#">Settings</a></li>
            <li class="nav-divider"></li>
            <li><a href="/auth">Logout</a></li>
        </ul>

    </div>
</div>

<!--main-->
<div class="container" id="main">


    <h3>Настройки сервера</h3>

    <!-- ALERT -->
    <% if(request.getAttribute("event") != null){%>
    <%=request.getAttribute("event")%>
    <% } %>
    <!-- ----- -->

    <div class="row">


            <div class="panel panel-default">
                <div class="panel-heading">Настройка оповещений</div>
                <div class="panel-body">

                    <form action="/admin/setting" method="post">
                        <h5>Включить оповещение о входе для администраторов</h5>
                        <label>Да
                            <input type="radio" name="notifyAdmins" id="notifyAdmins" value="1"
                                <%=request.getAttribute("notifyAdmins").equals(true)? "checked" : "" %>></label>
                        <label>Нет
                            <input type="radio" name="notifyAdmins" id="notifyAdmins" value="0"
                                <%=request.getAttribute("notifyAdmins").equals(false)? "checked" : "" %>></label>
                    <input type="submit" class="btn btn-success center-block" value="Сохранить изменения">
                    </form>
                </div>
            </div>



        <br>

        <div class="clearfix"></div>

        <hr>
        <div class="col-md-12 text-center"><p>Сайт создан Крикуновым Игорем<br>
            <a href="https://bitbucket.org/Igor2i" target="_ext">Мой Git</a></p></div>
        <hr>

    </div>
</div><!--/main-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/scripts.js"></script>
</body>
</html>

