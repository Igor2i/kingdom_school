<%--
  Created by IntelliJ IDEA.
  User: igor2i
  Date: 22.02.17
  Time: 18:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>KingDom School</title>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="/css/styles.css" rel="stylesheet">
</head>


<body>

<nav class="navbar navbar-fixed-top header">
    <div class="col-md-12">
        <div class="navbar-header">

            <a href="/"><img src="img/Logo.jpg" height="45px"></a>

        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" id="btnToggle" class="social-bar"><i class="glyphicon glyphicon-th-large"></i></a></li>
                <li><a href="#" class="social-bar"><img src="img/socail_f.jpg" height="35px"></a></li>
                <li><a href="#" class="social-bar"><img src="img/socail_t.jpg" height="35px"></a></li>
                <li><a href="#" class="social-bar"><img src="img/socail_i.jpg" height="35px"></a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="navbar navbar-default" id="subnav">
    <div class="col-md-12">
        <div class="navbar-header">

            <a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus dropdown-toggle"
               data-toggle="dropdown">&#9776; Home
                <%--<small><i class="glyphicon glyphicon-chevron-down"></i></small>--%>
            </a>
            <ul class="nav dropdown-menu">
                <li><a href="#"> Profile</a></li>
                <li><a href="#"> Dashboard</a></li>
                <li><a href="#"> Pages</a></li>
                <li class="nav-divider"></li>
                <li><a href="#"> Settings</a></li>
                <li><a href="#"> More..</a></li>
            </ul>


            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <%
            if (request.getSession().getAttribute("userKey") == null) {
        %>
        <div class="collapse navbar-collapse" id="navbar-collapse2">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#loginModal" role="button" data-toggle="modal">Login</a></li>
                <li><a href="/registration" role="button" data-toggle="modal">Registration</a></li>
                <li><a href="#aboutModal" role="button" data-toggle="modal">About</a></li>
            </ul>
        </div>
        <% } else { %>
        <%--<a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus dropdown-toggle" data-toggle="dropdown">--%>
        <a href="#" style="margin-left:15px;" class="navbar-btn btn btn-default btn-plus navbar-right dropdown-toggle"
           data-toggle="dropdown">
            <%="Hello " + request.getAttribute("userFIO")%> &#9660;
        </a>
        <ul class="nav dropdown-menu dropdown-menu-right">
            <li><a href="#">&#9998;Profile</a></li>
            <li><a href="#">Settings</a></li>
            <li class="nav-divider"></li>
            <li><a href="/auth">Logout</a></li>
        </ul>
        <% }%>

    </div>
</div>

<!--main-->
<div class="container" id="main">

    <div class="row">

        <div class="panel panel-default">
            <div class="panel-heading"><h3>Регистрация</h3></div>
            <!-- ALERT -->
            <% if(request.getAttribute("event") != null){%>
            <%=request.getAttribute("event")%>
            <% } %>
            <!-- ----- -->
            <div class="panel-body">
                <form action="/registration" method="post">
                    <table class="table">
                        <tr>
                            <td><label for="loginReg">Login:</label></td>
                            <td><input type="text" name="loginReg" id="loginReg" value="" placeholder="login"></td>
                        </tr>
                        <tr>
                            <td><label for="password">Password:</label></td>
                            <td><input type="password" name="password" id="password" value="" placeholder="password"></td>
                        </tr>
                        <tr>
                            <td><label for="email">Email:</label></td>
                            <td><input type="email" name="email" id="email" value="" placeholder="example@email.ru"></td>
                        </tr>
                        <tr>
                            <td><label for="lastname">Фамилия:</label></td>
                            <td><input type="text" name="lastname" id="lastname" value="" placeholder="Фамилия"></td>
                        </tr>
                        <tr>
                            <td><label for="firstname">Имя:</label></td>
                            <td><input type="text" name="firstname" id="firstname" value="" placeholder="Имя"></td>
                        </tr>
                        <tr>
                            <td><label for="dob">Дата рождения:</label></td>
                            <td><input type="date" name="dob" id="dob" value=""></td>
                        </tr>
                        <tr>
                            <td><label for="male">Пол:</label></td>
                            <td><label>Мужчина
                                <input type="radio" name="male" id="male" value="1"></label>
                                <label>Девушка
                                <input type="radio" name="male" id="male" value="0"></label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="vklink">VK link:</label></td>
                            <td><input type="text" name="vklink" id="vklink" value="" placeholder="Input"></td>
                        </tr>
                        <tr>
                            <td><label for="techer">Я хочу зарегистрироваться как преподаватель:</label></td>
                            <td><label>Нет, я хочу быть Учениеком
                                <input type="radio" name="techer" id="techer" value="0" checked></label><br>
                                <label>Да, стать Преподавателем
                                <input type="radio" name="techer" id="techer" value="1"></label>
                                </td>
                        </tr>
                        <tr>
                            <td><label for="verf">Я подтверждаю своё согласие на обработку ПД</label></td>
                            <td><label>Да
                                <input type="checkbox" name="verf" id="verf" value="1"></label></td>
                        </tr>
                    </table>


                    <br>

                    <input type="submit" class="btn btn-success center-block" value="Зарегистрироваться">
                </form>

            </div>
        </div>

        <br>

        <div class="clearfix"></div>

        <hr>
        <div class="col-md-12 text-center"><p>Сайт создан Крикуновым Игорем<br>
            <a href="https://bitbucket.org/Igor2i" target="_ext">Мой Git</a></p></div>
        <hr>

    </div>
</div><!--/main-->


<!--login modal-->
<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="text-center"><img
                        src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100"
                        class="img-circle"><br>Login</h2>
            </div>
            <div class="modal-body">
                <%
                    if (request.getSession().getAttribute("userKey") == null) {
                %>
                <form class="form col-md-12 center-block" action="/auth" method="post">
                    <div class="form-group">
                        <input type="text" name="login" id="login" class="form-control input-lg" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" name="passwd" id="passwd" class="form-control input-lg"
                               placeholder="Password">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Sign In</button>
                        <span class="pull-right"><a href="/auth">Register</a></span>
                    </div>
                </form>
                <% } else { %>
                <p><%="Hello " + request.getAttribute("userFIO")%>
                </p>
                <p><a href="/auth">logout</a></p>
                <% }%>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!--about modal-->
<div id="aboutModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="text-center">О нас</h2>
            </div>
            <div class="modal-body">
                <div class="col-md-12 text-center">
                    Данный ресурс предназначен для совместного изучения инностранных языков.
                    <br><br>
                    Желаем Вам успехов!
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">OK</button>
            </div>
        </div>
    </div>
</div>
<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/scripts.js"></script>
</body>
</html>

