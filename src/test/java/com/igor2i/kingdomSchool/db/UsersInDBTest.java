package com.igor2i.kingdomSchool.db;

import com.igor2i.kingdomSchool.db.modelsBO.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by igor2i on 28.02.17.
 */
public class UsersInDBTest {

    private static final Logger LOGGER = LogManager.getLogger(UsersInDBTest.class);


    @Test
    public void getAll() throws Exception {

        UsersInDB usersInDB = new UsersInDB();
        ConcurrentHashMap<Long, User> concurrentHashMap =  usersInDB.getAll();
        concurrentHashMap.forEach((aLong, user) -> System.out.println(user.toString()));
    }

    @Test
    public void setNewField() throws Exception {

        Random random = new Random();
        String login = "testUser"+ random.nextInt(10000);
        User user = new User(0,
                login,
                "passwd",
                "email@email.te",
                "userKey"+ random.nextInt(10000),
                false,
                false,
                5,
                login,
                login,
                new Date(2017,10,20),
                new Date(1990,10,20),
                true,
                "vk/" + login);


        UsersInDB usersInDB = new UsersInDB();
        System.out.println(usersInDB.setNewField(user).toString());
    }

    @Test
    public void getById() throws Exception {

        UsersInDB usersInDB = new UsersInDB();
        System.out.println(usersInDB.getById(8).toString());

    }

    @Test
    public void getByName() throws Exception {

        UsersInDB usersInDB = new UsersInDB();
        System.out.println(usersInDB.getByName("test123").toString());

    }

    @Test
    public void setUpdateById() throws Exception {

        UsersInDB usersInDB = new UsersInDB();
        User user1 = usersInDB.getById(8);

        LOGGER.info(user1.toString());

        Random random = new Random();
        user1.setLastName(user1.getLastName() + random.nextInt(1000));
        user1.setSocial(user1.getSocial() + random.nextInt(1000));
        usersInDB.setUpdateById(user1.getId(), user1);
        LOGGER.info("Update User " + user1.toString());

        User user2 = usersInDB.getById(8);

        LOGGER.info(user2.toString());

    }

    @Test
    public void importToDB() throws Exception {

    }



}